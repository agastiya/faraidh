import unittest

from faraidh.ashhaabul_furuudh import AshhaabulFuruudh
from faraidh.laws import IJTIMA, HADITS
from faraidh.person import Person
from faraidh.faraidh_settlement import FaraidhSettlement


class TestAshobah(unittest.TestCase):
    """http://media.isnet.org/kmi/islam/Waris/Macam.html"""

    def test_contoh_pertama(self):
        """
        Seseorang meninggal dunia dan meninggalkan anak perempuan, saudara perempuan, dan saudara laki-laki seayah
        """
        seseorang = Person(1, 'Seseorang', Person.SEX_MALE)
        istri = Person(2, 'Istri', Person.SEX_FEMALE)
        istri.died()

        anak_pr = Person(3, 'Anak Pr', Person.SEX_FEMALE)
        anak_pr.child_of(istri, seseorang)

        ibu = Person(5, 'Ibu', Person.SEX_FEMALE)
        ibu.died()
        ayah = Person(6, 'Ayah', Person.SEX_MALE)
        ayah.died()
        seseorang.child_of(ibu, ayah)
        saudari = Person(4, 'Saudari', Person.SEX_FEMALE)
        saudari.child_of(ibu, ayah)

        ibutiri = Person(7, 'Ibu tiri', Person.SEX_FEMALE)
        ibutiri.died()
        saudara = Person(8, 'Saudara', Person.SEX_MALE)
        saudara.child_of(ibutiri, ayah)

        afr = AshhaabulFuruudh(seseorang).find()
        self.assertEqual(2, len(afr))
        self.assertEqual(AshhaabulFuruudh.FARDH_ONE_HALF,
                         afr[AshhaabulFuruudh.TYPE_DAUGHTER][AshhaabulFuruudh.ASHHABFARDH_KEY_FARDH])
        self.assertEqual(AshhaabulFuruudh.FARDH_ONE_PART,
                         afr[AshhaabulFuruudh.TYPE_SISTER][AshhaabulFuruudh.ASHHABFARDH_KEY_ASHOBAH])
        self.assertIn(IJTIMA['ashobah-maal-ghair'],
                      afr[AshhaabulFuruudh.TYPE_SISTER][AshhaabulFuruudh.ASHHABFARDH_KEY_REASON])
        # saudara laki-laki seayah terhalang karena saudara kandung perempuan menjadi 'ashabah.
        self.assertNotIn(AshhaabulFuruudh.TYPE_PATERNAL_BROTHER, afr.keys())

        uf = FaraidhSettlement(afr)
        asal_masalah = uf.tashil()
        self.assertEqual(2, asal_masalah)
        siham = uf.share_ashhaabul_furuudh(asal_masalah)
        self.assertEqual((1, 2), siham[AshhaabulFuruudh.TYPE_DAUGHTER])
        self.assertEqual((1, 2), siham[FaraidhSettlement.TYPE_ASHOBAH_UNSHARED])
        siham = uf.share_ashobah()
        self.assertEqual((1, 1), siham[AshhaabulFuruudh.TYPE_SISTER])

    def test_contoh_kedua(self):
        """
        Seorang wanita meninggal dunia dan meninggalkan suami, cucu perempuan dari keturunan anak laki-laki,
        dua orang saudara kandung perempuan, dan saudara laki-laki seayah.
        """
        wanita = Person(1, 'Seorang wanita', Person.SEX_FEMALE)
        suami = Person(2, 'Suami', Person.SEX_MALE)
        anaklaki = Person(3, 'Anak laki-laki', Person.SEX_MALE)
        anaklaki.child_of(wanita, suami)
        anaklaki.died()
        istri_anaklaki = Person(4, 'Istri Anak laki-laki', Person.SEX_FEMALE)
        cucu_perempuan = Person(5, 'Cucu perempuan', Person.SEX_FEMALE)
        cucu_perempuan.child_of(istri_anaklaki, anaklaki)
        ibu = Person(6, 'Ibu', Person.SEX_FEMALE)
        ibu.died()
        ayah = Person(7, 'Ayah', Person.SEX_MALE)
        ayah.died()
        wanita.child_of(ibu, ayah)
        saudari1 = Person(8, 'Saudari 1', Person.SEX_FEMALE)
        saudari1.child_of(ibu, ayah)
        saudari2 = Person(8, 'Saudari 2', Person.SEX_FEMALE)
        saudari2.child_of(ibu, ayah)
        ibutiri = Person(9, 'Ibu tiri', Person.SEX_FEMALE)
        ibutiri.died()
        saudaratiri = Person(10, 'Saudara laki-laki seayah', Person.SEX_MALE)
        saudaratiri.child_of(ibutiri, ayah)

        afr = AshhaabulFuruudh(wanita).find()
        self.assertEqual(3, len(afr))
        self.assertEqual(AshhaabulFuruudh.FARDH_ONE_FORTH,
                         afr[AshhaabulFuruudh.TYPE_HUSBAND][AshhaabulFuruudh.ASHHABFARDH_KEY_FARDH])
        self.assertEqual(AshhaabulFuruudh.FARDH_ONE_HALF,
                         afr[AshhaabulFuruudh.TYPE_GRANDDAUGHTER][AshhaabulFuruudh.ASHHABFARDH_KEY_FARDH])
        self.assertEqual(AshhaabulFuruudh.FARDH_ONE_PART,
                         afr[AshhaabulFuruudh.TYPE_SISTER][AshhaabulFuruudh.ASHHABFARDH_KEY_ASHOBAH])
        self.assertIn(IJTIMA['ashobah-maal-ghair'],
                      afr[AshhaabulFuruudh.TYPE_SISTER][AshhaabulFuruudh.ASHHABFARDH_KEY_REASON])
        # bagian saudara laki-laki seayah gugur karena adanya dua saudara kandung.
        self.assertNotIn(AshhaabulFuruudh.TYPE_PATERNAL_BROTHER, afr.keys())

        uf = FaraidhSettlement(afr)
        asal_masalah = uf.tashil()
        self.assertEqual(4, asal_masalah)
        siham = uf.share_ashhaabul_furuudh(asal_masalah)
        self.assertEqual((1, 4), siham[AshhaabulFuruudh.TYPE_HUSBAND])
        self.assertEqual((2, 4), siham[AshhaabulFuruudh.TYPE_GRANDDAUGHTER])
        self.assertEqual((1, 4), siham[FaraidhSettlement.TYPE_ASHOBAH_UNSHARED])
        siham = uf.share_ashobah()
        self.assertEqual((1, 1), siham[AshhaabulFuruudh.TYPE_SISTER])

    def test_contoh_ketiga(self):
        """
        Seseorang meninggal dunia dan meninggalkan dua orang anak perempuan, saudara perempuan seayah,
        dan anak laki-laki saudara laki-laki (kemenakan)
        """
        seseorang = Person(1, 'Seseorang', Person.SEX_MALE)
        istri = Person(2, 'istri', Person.SEX_FEMALE)
        istri.died()
        anak1 = Person(3, 'anak 1', Person.SEX_FEMALE)
        anak1.child_of(istri, seseorang)
        anak2 = Person(4, 'anak2', Person.SEX_FEMALE)
        anak2.child_of(istri, seseorang)
        ayah = Person(5, 'ayah', Person.SEX_MALE)
        ayah.died()
        ibu = Person(6, 'ibu', Person.SEX_FEMALE)
        ibu.died()
        seseorang.child_of(ibu, ayah)
        ibutiri = Person(7, 'ibu tiri', Person.SEX_FEMALE)
        ibutiri.died()
        sister = Person(8, 'saudara perempuan seayah', Person.SEX_FEMALE)
        sister.child_of(ibutiri, ayah)
        brother = Person(9, 'saudara laki-laki', Person.SEX_MALE)
        brother.child_of(ibu, ayah)
        brother.died()
        brotherwife = Person(10, 'istri saudara laki-laki', Person.SEX_FEMALE)
        brotherwife.died()
        kemenakan = Person(11, 'kemenakan', Person.SEX_MALE)
        kemenakan.child_of(brotherwife, brother)

        afr = AshhaabulFuruudh(seseorang).find()
        self.assertEqual(2, len(afr))
        self.assertEqual(AshhaabulFuruudh.FARDH_TWO_THIRD,
                         afr[AshhaabulFuruudh.TYPE_DAUGHTER][AshhaabulFuruudh.ASHHABFARDH_KEY_FARDH])
        self.assertEqual(AshhaabulFuruudh.FARDH_ONE_PART,
                         afr[AshhaabulFuruudh.TYPE_PATERNAL_SISTER][AshhaabulFuruudh.ASHHABFARDH_KEY_ASHOBAH])
        self.assertIn(IJTIMA['ashobah-maal-ghair'],
                      afr[AshhaabulFuruudh.TYPE_PATERNAL_SISTER][AshhaabulFuruudh.ASHHABFARDH_KEY_REASON])
        self.assertNotIn(AshhaabulFuruudh.TYPE_SON_OF_BROTHER, afr.keys())

        uf = FaraidhSettlement(afr)
        asal_masalah = uf.tashil()
        self.assertEqual(3, asal_masalah)
        siham = uf.share_ashhaabul_furuudh(asal_masalah)
        self.assertEqual((2, 3), siham[AshhaabulFuruudh.TYPE_DAUGHTER])
        self.assertEqual((1, 3), siham[FaraidhSettlement.TYPE_ASHOBAH_UNSHARED])
        siham = uf.share_ashobah()
        self.assertEqual((1, 1), siham[AshhaabulFuruudh.TYPE_PATERNAL_SISTER])

    def test_contoh_keempat(self):
        """
        Seseorang meninggal dunia dan meninggalkan seorang anak perempuan, cucu perempuan keturunan anak laki-laki,
        seorang ibu, saudara perempuan seayah, dan paman kandung (saudara dari ayah kandung).
        """
        seseorang = Person(1, 'Seseorang', Person.SEX_MALE)
        istri = Person(2, 'Istri seseorang', Person.SEX_FEMALE)
        istri.died()
        daughter = Person(3, 'Anak perempuan', Person.SEX_FEMALE)
        daughter.child_of(istri, seseorang)
        son = Person(4, 'Anak laki-laki', Person.SEX_MALE)
        son.child_of(istri, seseorang)
        son.died()
        wife_son = Person(5, 'Istri anak laki-laki', Person.SEX_FEMALE)
        wife_son.died()
        granddaughter = Person(6, 'Cucu perempuan keturunan anak laki-laki', Person.SEX_FEMALE)
        granddaughter.child_of(wife_son, son)
        ayah = Person(7, 'Ayah', Person.SEX_MALE)
        ayah.died()
        ibu = Person(8, 'Ibu', Person.SEX_FEMALE)
        seseorang.child_of(ibu, ayah)
        ibutiri = Person(9, 'Ibu tiri', Person.SEX_FEMALE)
        ibutiri.died()
        sister = Person(10, 'Saudara perempuan', Person.SEX_FEMALE)
        sister.child_of(ibutiri, ayah)
        grandpa = Person(11, 'Ayah dari ayah kandung', Person.SEX_MALE)
        grandpa.died()
        grandma = Person(12, 'Ibu dari ayah kandung', Person.SEX_FEMALE)
        grandma.died()
        ayah.child_of(grandma, grandpa)
        paman = Person(13, 'Paman kandung', Person.SEX_MALE)
        paman.child_of(grandma, grandpa)

        afr = AshhaabulFuruudh(seseorang).find()
        self.assertEqual(4, len(afr))
        self.assertEqual(AshhaabulFuruudh.FARDH_ONE_HALF,
                         afr[AshhaabulFuruudh.TYPE_DAUGHTER][AshhaabulFuruudh.ASHHABFARDH_KEY_FARDH])
        self.assertEqual(AshhaabulFuruudh.FARDH_ONE_SIXTH,
                         afr[AshhaabulFuruudh.TYPE_GRANDDAUGHTER][AshhaabulFuruudh.ASHHABFARDH_KEY_FARDH])
        self.assertIn(HADITS['ibn-masud:1:3'],
                      afr[AshhaabulFuruudh.TYPE_GRANDDAUGHTER][AshhaabulFuruudh.ASHHABFARDH_KEY_REASON])
        self.assertEqual(AshhaabulFuruudh.FARDH_ONE_SIXTH,
                         afr[AshhaabulFuruudh.TYPE_MOTHER][AshhaabulFuruudh.ASHHABFARDH_KEY_FARDH])
        self.assertIn(IJTIMA['ashobah-maal-ghair'],
                      afr[AshhaabulFuruudh.TYPE_PATERNAL_SISTER][AshhaabulFuruudh.ASHHABFARDH_KEY_REASON])
        self.assertEqual(AshhaabulFuruudh.FARDH_ONE_PART,
                         afr[AshhaabulFuruudh.TYPE_PATERNAL_SISTER][AshhaabulFuruudh.ASHHABFARDH_KEY_ASHOBAH])
        self.assertNotIn(AshhaabulFuruudh.TYPE_FATHERS_PATERNAL_BROTHER, afr.keys())

        uf = FaraidhSettlement(afr)
        asal_masalah = uf.tashil()
        self.assertEqual(6, asal_masalah)
        siham = uf.share_ashhaabul_furuudh(asal_masalah)
        self.assertEqual((3, 6), siham[AshhaabulFuruudh.TYPE_DAUGHTER])
        self.assertEqual((1, 6), siham[AshhaabulFuruudh.TYPE_GRANDDAUGHTER])
        self.assertEqual((1, 6), siham[AshhaabulFuruudh.TYPE_MOTHER])
        self.assertEqual((1, 6), siham[FaraidhSettlement.TYPE_ASHOBAH_UNSHARED])
        siham = uf.share_ashobah()
        self.assertEqual((1, 1), siham[AshhaabulFuruudh.TYPE_PATERNAL_SISTER])
