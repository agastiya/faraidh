import unittest

from faraidh.ashhaabul_furuudh import AshhaabulFuruudh
from faraidh.person import Person
from faraidh.faraidh_settlement import FaraidhSettlement
from faraidh.laws import IJTIMA


class TestRumaysho(unittest.TestCase):
    """@see https://rumaysho.com/2502-panduan-ringkas-ilmu-waris.html"""

    def test_soal1(self):
        """
        Seorang laki-laki meninggal dunia dengan meninggalkan  1 orang istri , 1 orang anak laki-laki
        dan 1 orang anak perempuan dari anak laki-laki.
        """

        # seorang laki-laki meninggal dunia,
        person = Person(1, "Person", Person.SEX_MALE)

        # dengan meninggalkan 1 orang istri
        wife = Person(2, "Wife", Person.SEX_FEMALE)
        person.married(wife)

        # 1 orang anak laki-laki, dan
        son = Person(12, "Son", Person.SEX_MALE)
        son.child_of(wife, person)

        # 1 orang anak perempuan dari anak laki-laki
        son_wife = Person(13, "Son's Wife", Person.SEX_FEMALE)
        granddaughter = Person(21, "Anak perempuan dari anak laki", Person.SEX_FEMALE)
        granddaughter.child_of(son_wife, son)
        son_wife.died()

        af = AshhaabulFuruudh(person)
        heirs = af.find()
        self.assertCountEqual([
            AshhaabulFuruudh.TYPE_WIFE,
            AshhaabulFuruudh.TYPE_SON
        ], heirs.keys())
        self.assertEqual(af.FARDH_ONE_EIGHTH, heirs[af.TYPE_WIFE][af.ASHHABFARDH_KEY_FARDH])
        self.assertIsNone(heirs[af.TYPE_SON][af.ASHHABFARDH_KEY_FARDH])
        self.assertEqual(AshhaabulFuruudh.FARDH_TWO_PART, heirs[af.TYPE_SON][af.ASHHABFARDH_KEY_ASHOBAH])

        uf = FaraidhSettlement(heirs)
        asal_masalah = uf.tashil()
        self.assertEqual(8, asal_masalah)
        points = uf.share_ashhaabul_furuudh(asal_masalah)
        self.assertEqual((1, 8), points[AshhaabulFuruudh.TYPE_WIFE])
        self.assertEqual((7, 8), points[FaraidhSettlement.TYPE_ASHOBAH_UNSHARED])
        points = uf.share_ashobah()
        self.assertEqual((2, 2), points[AshhaabulFuruudh.TYPE_SON])

    def test_soal2(self):
        """
        Seorang laki-laki meninggal dunia dan meninggalkan 1 anak perempuan dan seorang ayah.
        """

        # Seorang laki-laki meninggal dunia, dan
        person = Person(1, "Person", Person.SEX_MALE)

        # meninggalkan 1 anak perempuan, dan
        wife = Person(2, "Wife", Person.SEX_FEMALE)
        person.married(wife)
        wife.died()
        daughter = Person(3, "Daughter", Person.SEX_FEMALE)
        daughter.child_of(wife, person)

        # seorang ayah
        dad = Person(4, "Dad", Person.SEX_MALE)
        mom = Person(5, "Mom", Person.SEX_FEMALE)
        person.child_of(mom, dad)
        mom.died()

        af = AshhaabulFuruudh(person)
        heirs = af.find()
        self.assertCountEqual([
            AshhaabulFuruudh.TYPE_DAUGHTER,
            AshhaabulFuruudh.TYPE_FATHER
        ], heirs.keys())
        self.assertEqual(af.FARDH_ONE_HALF, heirs[af.TYPE_DAUGHTER][af.ASHHABFARDH_KEY_FARDH])

        # ayah: 1/6 + sisa
        self.assertEqual(af.FARDH_ONE_SIXTH, heirs[af.TYPE_FATHER][af.ASHHABFARDH_KEY_FARDH])
        self.assertEqual(AshhaabulFuruudh.FARDH_TWO_PART, heirs[af.TYPE_FATHER][af.ASHHABFARDH_KEY_ASHOBAH])

        uf = FaraidhSettlement(heirs)
        asal_masalah = uf.tashil()
        self.assertEqual(6, asal_masalah)
        points = uf.share_ashhaabul_furuudh(asal_masalah)
        self.assertEqual((3, 6), points[AshhaabulFuruudh.TYPE_DAUGHTER])
        self.assertEqual((1, 6), points[AshhaabulFuruudh.TYPE_FATHER])
        self.assertEqual((2, 6), points[FaraidhSettlement.TYPE_ASHOBAH_UNSHARED])
        points = uf.share_ashobah()
        self.assertEqual((2, 2), points[AshhaabulFuruudh.TYPE_FATHER])

    @unittest.skip('not sure why granddaughter do not become ashobah bi ghairihi together with cicit laki-laki')
    def test_soal3(self):
        """
        Seorang wanita meninggal dunia dengan meninggalkan seorang suami, 1 anak perempuan, 1 anak perempuan dari
        anak laki-laki, 1 anak laki-laki dari anak laki-laki dari anak laki-laki (cicit).
        """

        # Seorang wanita meninggal dunia dengan meninggalkan seorang suami
        person = Person(1, "Person", Person.SEX_FEMALE)
        husband = Person(2, "Husband", Person.SEX_MALE)
        person.married(husband)

        # 1 anak perempuan
        daughter = Person(3, "Daughter", Person.SEX_FEMALE)
        daughter.child_of(person, husband)

        # 1 anak perempuan dari anak laki-laki
        son = Person(4, "Son", Person.SEX_MALE)
        son.child_of(person, husband)
        son.died()
        son_wife = Person(5, "Son's wife", Person.SEX_FEMALE)
        son_wife.died()
        granddaughter = Person(6, "Granddaughter", Person.SEX_FEMALE)
        granddaughter.child_of(son_wife, son)

        # 1 anak laki-laki dari anak laki-laki dari anak laki-laki (cicit).
        grandson = Person(7, "Grandson", Person.SEX_MALE)
        grandson.child_of(son_wife, son)
        grandson.died()
        grandson_wife = Person(8, "Grandson's wife", Person.SEX_FEMALE)
        grandson_wife.died()
        greatson = Person(9, "Greatson", Person.SEX_MALE)
        greatson.child_of(grandson_wife, grandson)

        af = AshhaabulFuruudh(person)
        heirs = af.find()
        self.assertCountEqual([
            AshhaabulFuruudh.TYPE_HUSBAND,
            AshhaabulFuruudh.TYPE_DAUGHTER,
            AshhaabulFuruudh.TYPE_GRANDDAUGHTER,
            AshhaabulFuruudh.TYPE_GRANDSON
        ], heirs.keys())
        self.assertEqual(af.FARDH_ONE_FORTH, heirs[af.TYPE_HUSBAND][af.ASHHABFARDH_KEY_FARDH])
        self.assertEqual(af.FARDH_ONE_HALF, heirs[af.TYPE_DAUGHTER][af.ASHHABFARDH_KEY_FARDH])

        # FIXME: cicit laki-laki mempengaruhi anak perempuan dari anak laki-laki menjadi ashobah bi ghairihi?
        # self.assertEqual(af.FARDH_ONE_SIXTH, heirs[af.TYPE_GRANDDAUGHTER][af.ASHHABFARDH_KEY_FARDH])
        self.assertEqual(af.FARDH_ONE_PART, heirs[af.TYPE_GRANDDAUGHTER][af.ASHHABFARDH_KEY_ASHOBAH])
        self.assertIn(IJTIMA['ashobah-bi-ghairihi:2'],
                      heirs[af.TYPE_GRANDDAUGHTER][af.ASHHABFARDH_KEY_REASON])

        self.assertIsNone(heirs[af.TYPE_GRANDSON][af.ASHHABFARDH_KEY_FARDH])
        self.assertEqual(af.FARDH_TWO_PART, heirs[af.TYPE_GRANDSON][af.ASHHABFARDH_KEY_ASHOBAH])

        uf = FaraidhSettlement(heirs)
        asal_masalah = uf.tashil()
        self.assertEqual(12, asal_masalah)
        points = uf.share_ashhaabul_furuudh(asal_masalah)
        self.assertEqual((3, 12), points[AshhaabulFuruudh.TYPE_HUSBAND])
        self.assertEqual((6, 12), points[AshhaabulFuruudh.TYPE_DAUGHTER])
        self.assertEqual((2, 12), points[AshhaabulFuruudh.TYPE_GRANDDAUGHTER])
        self.assertEqual((1, 12), points[FaraidhSettlement.TYPE_ASHOBAH_UNSHARED])

    def test_soal4(self):
        """
        Seorang pria meninggal dunia meninggalkan seorang ibu, seorang saudara kandung wanita dan seorang paman
        """

        # Seorang pria meninggal dunia
        person = Person(1, "Person", Person.SEX_MALE)

        # meninggalkan seorang ibu,
        dad = Person(4, "Dad", Person.SEX_MALE)
        mom = Person(5, "Mom", Person.SEX_FEMALE)
        person.child_of(mom, dad)
        dad.died()

        # seorang saudara kandung wanita, dan
        sister = Person(6, "Sister", Person.SEX_FEMALE)
        sister.child_of(mom, dad)

        # seorang paman
        grandpa = Person(7, "Grandpa", Person.SEX_MALE)
        grandma = Person(8, "Grandma", Person.SEX_FEMALE)
        dad.child_of(grandma, grandpa)
        uncle = Person(9, "Uncle", Person.SEX_MALE)
        uncle.child_of(grandma, grandpa)
        grandpa.died()
        grandma.died()

        af = AshhaabulFuruudh(person)
        heirs = af.find()
        self.assertCountEqual([
            AshhaabulFuruudh.TYPE_MOTHER,
            AshhaabulFuruudh.TYPE_SISTER,
            AshhaabulFuruudh.TYPE_FATHERS_BROTHER
        ], heirs.keys())
        self.assertEqual(af.FARDH_ONE_THIRD, heirs[af.TYPE_MOTHER][af.ASHHABFARDH_KEY_FARDH])
        self.assertEqual(af.FARDH_ONE_HALF, heirs[af.TYPE_SISTER][af.ASHHABFARDH_KEY_FARDH])

        # uncle ashobah
        self.assertIsNone(heirs[af.TYPE_FATHERS_BROTHER][af.ASHHABFARDH_KEY_FARDH])
        self.assertEqual(af.FARDH_TWO_PART, heirs[af.TYPE_FATHERS_BROTHER][af.ASHHABFARDH_KEY_ASHOBAH])

        uf = FaraidhSettlement(heirs)
        asal_masalah = uf.tashil()
        self.assertEqual(6, asal_masalah)
        points = uf.share_ashhaabul_furuudh(asal_masalah)
        self.assertEqual((2, 6), points[af.TYPE_MOTHER])
        self.assertEqual((3, 6), points[af.TYPE_SISTER])
        self.assertEqual((1, 6), points[FaraidhSettlement.TYPE_ASHOBAH_UNSHARED])
        points = uf.share_ashobah()
        self.assertEqual((2, 2), points[af.TYPE_FATHERS_BROTHER])

    def test_soal5(self):
        """
        Seorang pria meninggal dunia dengan meninggalkan seorang  ibu, seorang ayah, anak laki-laki, saudara kandung
        laki-laki
        """

        # Seorang pria meninggal dunia
        person = Person(1, "Person", Person.SEX_MALE)

        # meninggalkan seorang ibu, seorang ayah,
        dad = Person(4, "Dad", Person.SEX_MALE)
        mom = Person(5, "Mom", Person.SEX_FEMALE)
        person.child_of(mom, dad)

        # anak laki-laki,
        wife = Person(2, "Wife", Person.SEX_FEMALE)
        person.married(wife)
        wife.died()
        son = Person(3, "Son", Person.SEX_MALE)
        son.child_of(wife, person)

        # saudara kandung laki-laki
        brother = Person(6, "Brother", Person.SEX_MALE)
        brother.child_of(mom, dad)

        af = AshhaabulFuruudh(person)
        heirs = af.find()
        self.assertCountEqual([
            AshhaabulFuruudh.TYPE_MOTHER,
            AshhaabulFuruudh.TYPE_FATHER,
            AshhaabulFuruudh.TYPE_SON
        ], heirs.keys())
        self.assertEqual(af.FARDH_ONE_SIXTH, heirs[af.TYPE_MOTHER][af.ASHHABFARDH_KEY_FARDH])
        self.assertEqual(af.FARDH_ONE_SIXTH, heirs[af.TYPE_FATHER][af.ASHHABFARDH_KEY_FARDH])

        # anak laki-laki ashobah
        self.assertIsNone(heirs[af.TYPE_SON][af.ASHHABFARDH_KEY_FARDH])
        self.assertEqual(af.FARDH_TWO_PART, heirs[af.TYPE_SON][af.ASHHABFARDH_KEY_ASHOBAH])

        uf = FaraidhSettlement(heirs)
        asal_masalah = uf.tashil()
        self.assertEqual(6, asal_masalah)
        points = uf.share_ashhaabul_furuudh(asal_masalah)
        self.assertEqual((1, 6), points[af.TYPE_MOTHER])
        self.assertEqual((1, 6), points[af.TYPE_FATHER])
        self.assertEqual((4, 6), points[FaraidhSettlement.TYPE_ASHOBAH_UNSHARED])
        points = uf.share_ashobah()
        self.assertEqual((2, 2), points[af.TYPE_SON])

    def test_soal6(self):
        """
        Seorang pria meninggal dunia dan meninggalkan 2 anak laki-laki, 1 anak laki-laki dari anak laki-laki (cucu),
        ayah, kakek dan nenek.
        """

        # Seorang pria meninggal dunia dan
        person = Person(1, "Person", Person.SEX_MALE)

        # meninggalkan 2 anak laki-laki,
        wife = Person(2, "Wife", Person.SEX_FEMALE)
        person.married(wife)
        wife.died()
        son1 = Person(3, "Son 1", Person.SEX_MALE)
        son1.child_of(wife, person)
        son2 = Person(4, "Son 2", Person.SEX_MALE)
        son2.child_of(wife, person)

        # 1 anak laki-laki dari anak laki-laki (cucu),
        son1_wife = Person(5, "Son 1's wife", Person.SEX_FEMALE)
        son1_wife.died()
        grandson = Person(6, "Grandson", Person.SEX_MALE)
        grandson.child_of(son1_wife, son1)

        # ayah,
        dad = Person(7, "Dad", Person.SEX_MALE)
        mom = Person(8, "Mom", Person.SEX_FEMALE)
        mom.died()
        person.child_of(mom, dad)

        # kakek, dan
        grandpa = Person(9, "Grandpa", Person.SEX_MALE)
        grandpa_wife = Person(10, "Grandpa's wife", Person.SEX_FEMALE)
        dad.child_of(grandpa_wife, grandpa)
        grandpa_wife.died()

        # nenek
        grandma = Person(11, "Grandma", Person.SEX_FEMALE)
        grandma_husband = Person(12, "Grandma's husband", Person.SEX_MALE)
        mom.child_of(grandma, grandma_husband)
        grandma_husband.died()

        af = AshhaabulFuruudh(person)
        heirs = af.find()
        self.assertCountEqual([
            AshhaabulFuruudh.TYPE_FATHER,
            AshhaabulFuruudh.TYPE_GRANDMOTHER,
            AshhaabulFuruudh.TYPE_SON
        ], heirs.keys())
        self.assertEqual(af.FARDH_ONE_SIXTH, heirs[af.TYPE_FATHER][af.ASHHABFARDH_KEY_FARDH])
        self.assertEqual(af.FARDH_ONE_SIXTH, heirs[af.TYPE_GRANDMOTHER][af.ASHHABFARDH_KEY_FARDH])

        # son ashobah
        self.assertIsNone(heirs[af.TYPE_SON][af.ASHHABFARDH_KEY_FARDH])
        self.assertEqual(af.FARDH_TWO_PART, heirs[af.TYPE_SON][af.ASHHABFARDH_KEY_ASHOBAH])

        uf = FaraidhSettlement(heirs)
        asal_masalah = uf.tashil()
        self.assertEqual(6, asal_masalah)
        points = uf.share_ashhaabul_furuudh(asal_masalah)
        self.assertEqual((1, 6), points[af.TYPE_FATHER])
        self.assertEqual((1, 6), points[af.TYPE_GRANDMOTHER])
        self.assertEqual((4, 6), points[FaraidhSettlement.TYPE_ASHOBAH_UNSHARED])
        points = uf.share_ashobah()
        self.assertEqual((2, 2), points[af.TYPE_SON])

    def test_soal7(self):
        """
        Seorang pria meninggal dunia dan meninggalkan ayah, 1 anak perempuan, 1 anak laki-laki, 1 paman, 1 kakek,
        1 anak perempuan dari anak laki-laki.
        """

        # Seorang pria meninggal dunia dan
        person = Person(1, "Person", Person.SEX_MALE)

        # meninggalkan ayah,
        dad = Person(7, "Dad", Person.SEX_MALE)
        mom = Person(8, "Mom", Person.SEX_FEMALE)
        mom.died()
        person.child_of(mom, dad)

        # 1 anak perempuan
        wife = Person(2, "Wife", Person.SEX_FEMALE)
        person.married(wife)
        wife.died()
        daughter = Person(4, "Daughter", Person.SEX_FEMALE)
        daughter.child_of(wife, person)

        # 1 anak laki-laki,
        son = Person(3, "Son", Person.SEX_MALE)
        son.child_of(wife, person)

        # 1 paman, 1 kakek,
        grandpa = Person(9, "Grandpa", Person.SEX_MALE)
        grandpa_wife = Person(10, "Grandpa's wife", Person.SEX_FEMALE)
        dad.child_of(grandpa_wife, grandpa)
        uncle = Person(11, "Uncle", Person.SEX_MALE)
        uncle.child_of(grandpa_wife, grandpa)
        grandpa_wife.died()

        # 1 anak perempuan dari anak laki-laki
        son_wife = Person(5, "Son's wife", Person.SEX_FEMALE)
        son_wife.died()
        granddaughter = Person(12, "Granddaughter", Person.SEX_FEMALE)
        granddaughter.child_of(son_wife, son)

        af = AshhaabulFuruudh(person)
        heirs = af.find()
        self.assertCountEqual([
            AshhaabulFuruudh.TYPE_FATHER,
            AshhaabulFuruudh.TYPE_SON,
            AshhaabulFuruudh.TYPE_DAUGHTER
        ], heirs.keys())
        self.assertEqual(af.FARDH_ONE_SIXTH, heirs[af.TYPE_FATHER][af.ASHHABFARDH_KEY_FARDH])

        # anak laki dan perempuan ashobah
        self.assertIsNone(heirs[af.TYPE_SON][af.ASHHABFARDH_KEY_FARDH])
        self.assertEqual(af.FARDH_TWO_PART, heirs[af.TYPE_SON][af.ASHHABFARDH_KEY_ASHOBAH])
        self.assertIsNone(heirs[af.TYPE_DAUGHTER][af.ASHHABFARDH_KEY_FARDH])
        self.assertEqual(af.FARDH_ONE_PART, heirs[af.TYPE_DAUGHTER][af.ASHHABFARDH_KEY_ASHOBAH])

        uf = FaraidhSettlement(heirs)
        asal_masalah = uf.tashil()
        self.assertEqual(6, asal_masalah)
        points = uf.share_ashhaabul_furuudh(asal_masalah)
        self.assertEqual((1, 6), points[af.TYPE_FATHER])
        self.assertEqual((5, 6), points[FaraidhSettlement.TYPE_ASHOBAH_UNSHARED])
        points = uf.share_ashobah()
        self.assertEqual((2, 3), points[af.TYPE_SON])
        self.assertEqual((1, 3), points[af.TYPE_DAUGHTER])

    def test_soal8(self):
        """
        Seorang pria meninggal dunia dan meninggalkan 1 anak perempuan, 1 saudara perempuan seayah, 1 anak laki-laki
        dari saudara laki-laki seayah, 1 saudara laki-laki seibu.
        """

        # Seorang pria meninggal dunia dan
        person = Person(1, "Person", Person.SEX_MALE)

        # 1 anak perempuan
        wife = Person(2, "Wife", Person.SEX_FEMALE)
        person.married(wife)
        wife.died()
        daughter = Person(4, "Daughter", Person.SEX_FEMALE)
        daughter.child_of(wife, person)

        # 1 saudara perempuan seayah
        dad = Person(9, "Dad", Person.SEX_MALE)
        mom = Person(10, "Mom", Person.SEX_FEMALE)
        dad.died()
        mom.died()
        person.child_of(mom, dad)
        dad_wife = Person(11, "Dad's wife", Person.SEX_FEMALE)
        dad_wife.died()
        sister = Person(12, "Sister seayah", Person.SEX_FEMALE)
        sister.child_of(dad_wife, dad)

        # 1 anak laki-laki dari saudara laki-laki seayah
        brother = Person(13, "Brother", Person.SEX_MALE)
        brother.child_of(dad_wife, dad)
        brother.died()
        brother_wife = Person(14, "Brother's wife", Person.SEX_FEMALE)
        brother_wife.died()
        nephew = Person(14, "Nephew", Person.SEX_MALE)
        nephew.child_of(brother_wife, brother)

        # 1 saudara laki-laki seibu
        mom_husband = Person(15, "Mom's husband", Person.SEX_MALE)
        mom_husband.died()
        brother2 = Person(16, "Brother 2", Person.SEX_MALE)
        brother2.child_of(mom, mom_husband)

        af = AshhaabulFuruudh(person)
        heirs = af.find()
        self.assertCountEqual([
            AshhaabulFuruudh.TYPE_DAUGHTER,
            AshhaabulFuruudh.TYPE_PATERNAL_SISTER
        ], heirs.keys())
        self.assertEqual(af.FARDH_ONE_HALF, heirs[af.TYPE_DAUGHTER][af.ASHHABFARDH_KEY_FARDH])

        # saudara perempuan seayah ashobah
        self.assertIsNone(heirs[af.TYPE_PATERNAL_SISTER][af.ASHHABFARDH_KEY_FARDH])
        self.assertEqual(af.FARDH_ONE_PART, heirs[af.TYPE_PATERNAL_SISTER][af.ASHHABFARDH_KEY_ASHOBAH])

        uf = FaraidhSettlement(heirs)
        asal_masalah = uf.tashil()
        self.assertEqual(2, asal_masalah)
        points = uf.share_ashhaabul_furuudh(asal_masalah)
        self.assertEqual((1, 2), points[af.TYPE_DAUGHTER])
        self.assertEqual((1, 2), points[FaraidhSettlement.TYPE_ASHOBAH_UNSHARED])
        points = uf.share_ashobah()
        self.assertEqual((1, 1), points[af.TYPE_PATERNAL_SISTER])
