from faraidh.person import Person

import unittest


class PersonTests(unittest.TestCase):

    def test_init(self):
        person = Person(10, "Egi", Person.SEX_MALE)
        self.assertEqual(10, person.get_key())
        self.assertEqual("Egi", person.get_name())
        self.assertEqual(Person.SEX_MALE, person.get_sex())

    def test_parent(self):
        child = Person(10, "Child", Person.SEX_MALE)
        mom   = Person(11, "Mom", Person.SEX_FEMALE)
        dad   = Person(12, "Dad", Person.SEX_MALE)
        child.child_of(mom, dad)
        self.assertEqual(11, child.get_mother().get_key())
        self.assertEqual(12, child.get_father().get_key())
        self.assertEqual([child], mom.get_children())
        self.assertEqual([child], dad.get_children())

    def test_spouse(self):
        person = Person(10, "Person", Person.SEX_FEMALE)
        husband = Person(11, "Husband", Person.SEX_MALE)
        person.married(husband)
        self.assertEqual("Husband", person.get_spouses()[0].get_name())
        self.assertEqual(1, len(person.get_spouses()))
        self.assertEqual("Person", husband.get_spouses()[0].get_name())
        self.assertEqual(1, len(husband.get_spouses()))
        self.assertEqual("Husband", person.get_husbands()[0].get_name())
        self.assertEqual(1, len(person.get_husbands()))
        self.assertFalse(person.get_wives())

    def test_dead(self):
        child = Person(10, "Child", Person.SEX_MALE)
        self.assertTrue(child.is_alive())
        child.died()
        self.assertFalse(child.is_alive())

    def test_son(self):
        son = Person(10, "Son", Person.SEX_MALE)
        mom = Person(11, "Mom", Person.SEX_FEMALE)
        dad = Person(12, "Dad", Person.SEX_MALE)
        son.child_of(mom, dad)

        daughter = Person(13, "Daughter", Person.SEX_FEMALE)
        daughter.child_of(mom, dad)

        self.assertEqual([son], dad.get_sons())
        self.assertEqual([daughter], dad.get_daughters())


if __name__ == '__main__':
    unittest.main()
