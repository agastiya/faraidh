from faraidh.ashhaabul_furuudh import AshhaabulFuruudh, UnableToFindLivingRelativeError
from faraidh.person import Person

import unittest


class TestAshhaabulFuruudh(unittest.TestCase):

    def test_ushul_male(self):
        person = Person(1, "Person", Person.SEX_FEMALE)

        mom = Person(5, "Mom", Person.SEX_FEMALE)
        dad = Person(4, "Dad", Person.SEX_MALE)
        person.child_of(mom, dad)

        grandmom = Person(6, "Grand Mom", Person.SEX_FEMALE)
        granddad = Person(7, "Grand Dad", Person.SEX_MALE)
        dad.child_of(grandmom, granddad)

        greatmom = Person(6, "Great Mom", Person.SEX_FEMALE)
        greatdad = Person(7, "Great Dad", Person.SEX_MALE)
        granddad.child_of(greatmom, greatdad)

        af = AshhaabulFuruudh(person)
        af.find()
        heir, part, ashobah, _ = af.handle_father()
        self.assertEqual("Dad", heir.get_name())
        self.assertIsNone(part)
        self.assertTrue(ashobah)

        dad.died()
        af.find()
        with self.assertRaises(UnableToFindLivingRelativeError):
            af.handle_father()
        heir, part, ashobah, _ = af.handle_grandfathers()
        self.assertEqual("Grand Dad", heir.get_name())
        self.assertIsNone(part)
        self.assertTrue(ashobah)

        granddad.died()
        af.find()
        heir, part, ashobah, _ = af.handle_grandfathers()
        self.assertEqual("Great Dad", heir.get_name())
        self.assertIsNone(part)
        self.assertTrue(ashobah)

        greatdad.died()
        af.find()
        with self.assertRaises(UnableToFindLivingRelativeError):
            af.handle_grandfathers()

    def test_furu_male(self):
        person = Person(1, "Person", Person.SEX_FEMALE)
        husband = Person(2, "Husband", Person.SEX_MALE)

        son = Person(3, "Son", Person.SEX_MALE)
        son_wife = Person(4, "Son's Wife", Person.SEX_FEMALE)
        son.child_of(person, husband)

        grandson = Person(5, "Grandson", Person.SEX_MALE)
        grandson_wife = Person(6, "Grandson's Wife", Person.SEX_FEMALE)
        grandson.child_of(son_wife, son)

        greatson = Person(7, "Greatson", Person.SEX_MALE)
        greatson.child_of(grandson_wife, grandson)

        af = AshhaabulFuruudh(person)
        heirs, part, ashobah, _ = af.handle_sons()
        self.assertEqual("Son", heirs[0].get_name())
        self.assertIsNone(part)
        self.assertTrue(ashobah)

        son.died()
        with self.assertRaises(UnableToFindLivingRelativeError):
            af.handle_sons()
            self.assertEqual("Grandson", af.handle_grandsons()[af.ASHHABFARDH_KEY_HEIRS][0].get_name())

            grandson.died()
            self.assertEqual("Greatson", af.handle_grandsons()[af.ASHHABFARDH_KEY_HEIRS][0].get_name())

            greatson.died()
            af.handle_grandsons()

    def test_get_husbands(self):
        person = Person(1, "Person", Person.SEX_FEMALE)
        husband = Person(2, "Husband", Person.SEX_MALE)
        person.married(husband)

        af = AshhaabulFuruudh(person)
        af.find()
        heir, part, unused_is_ashobah, _ = af.handle_husbands()
        self.assertEqual("Husband", heir[0].get_name())
        self.assertEqual(AshhaabulFuruudh.FARDH_ONE_HALF, part)

        son = Person(3, "Son", Person.SEX_MALE)
        son.child_of(person, husband)

        af.find()
        heir, part, unused_is_ashobah, _ = af.handle_husbands()
        self.assertEqual("Husband", heir[0].get_name())
        self.assertEqual(AshhaabulFuruudh.FARDH_ONE_FORTH, part)

        husband.died()
        with self.assertRaises(UnableToFindLivingRelativeError):
            af.handle_husbands()

    def test_get_brothers(self):
        dad0 = Person(1, "Dad", Person.SEX_MALE)
        mom1 = Person(2, "Mom", Person.SEX_FEMALE)
        dad1 = Person(3, "Dad", Person.SEX_MALE)
        mom2 = Person(4, "Mom", Person.SEX_FEMALE)
        mom1.married(dad0)
        mom1.married(dad1)
        mom2.married(dad1)

        brother1 = Person(11, "Brother1", Person.SEX_MALE)
        brother1.child_of(mom1, dad0)
        brother2 = Person(12, "Brother2", Person.SEX_MALE)
        brother2.child_of(mom1, dad1)
        brother3 = Person(13, "Brother3", Person.SEX_MALE)
        brother3.child_of(mom2, dad1)

        person = Person(14, "Person", Person.SEX_FEMALE)
        person.child_of(mom1, dad1)

        brother1wife = Person(31, "Brother1 Wife", Person.SEX_FEMALE)
        son1 = Person(21, "Son1", Person.SEX_MALE)
        son1.child_of(brother1wife, brother1)

        brother2wife = Person(32, "Brother2 Wife", Person.SEX_FEMALE)
        son2 = Person(22, "Son2", Person.SEX_MALE)
        son2.child_of(brother2wife, brother2)

        brother3wife = Person(33, "Brother3 Wife", Person.SEX_FEMALE)
        son3 = Person(23, "Son3", Person.SEX_MALE)
        son3.child_of(brother3wife, brother3)

        af = AshhaabulFuruudh(person)
        af.find()
        heirs, unused_part, unused_ashobah, _ = af.handle_brothers()
        self.assertEqual("Brother2", heirs[0].get_name())
        self.assertEqual(1, len(heirs))
        self.assertCountEqual([brother3], af.handle_paternal_brothers()[af.ASHHABFARDH_KEY_HEIRS])
        self.assertCountEqual([brother1], af.handle_maternal_brothers()[af.ASHHABFARDH_KEY_HEIRS])

        self.assertEqual("Son2", af.handle_son_of_brothers()[af.ASHHABFARDH_KEY_HEIRS][0].get_name())
        paternal_brothers = af.handle_son_of_paternal_brothers()
        self.assertCountEqual([son3], paternal_brothers[af.ASHHABFARDH_KEY_HEIRS])

    def test_get_sisters(self):
        dad0 = Person(1, "Dad", Person.SEX_MALE)
        mom1 = Person(2, "Mom", Person.SEX_FEMALE)
        dad1 = Person(3, "Dad", Person.SEX_MALE)
        mom2 = Person(4, "Mom", Person.SEX_FEMALE)
        mom1.married(dad0)
        mom1.married(dad1)
        mom2.married(dad1)

        sister1 = Person(11, "Sister1", Person.SEX_FEMALE)
        sister1.child_of(mom1, dad0)
        sister2 = Person(12, "Sister2", Person.SEX_FEMALE)
        sister2.child_of(mom1, dad1)
        sister3 = Person(13, "Sister3", Person.SEX_FEMALE)
        sister3.child_of(mom2, dad1)

        person = Person(14, "Person", Person.SEX_MALE)
        person.child_of(mom1, dad1)

        af = AshhaabulFuruudh(person)
        heirs, part, unused_is_ashobah, _ = af.handle_sisters()
        self.assertCountEqual([sister2], heirs)
        self.assertEqual("Sister2", heirs[0].get_name())
        self.assertEqual(1, len(heirs))
        self.assertEqual(AshhaabulFuruudh.FARDH_ONE_HALF, part)

        heirs, part, unused_is_ashobah, _ = af.handle_paternal_sisters()
        self.assertCountEqual([sister3], heirs)
        self.assertEqual(AshhaabulFuruudh.FARDH_ONE_HALF, part)

        self.assertCountEqual([sister1], af.handle_maternal_sisters()[af.ASHHABFARDH_KEY_HEIRS])

        sister4 = Person(15, "Sister4", Person.SEX_FEMALE)
        sister4.child_of(mom1, dad1)
        heirs, part, unused_is_ashobah, _ = af.handle_sisters()
        self.assertEqual(2, len(heirs))
        self.assertEqual(AshhaabulFuruudh.FARDH_TWO_THIRD, part)

        sister2.died()
        sister4.died()
        with self.assertRaises(UnableToFindLivingRelativeError):
            af.handle_sisters()

        heirs, part, unused_is_ashobah, _ = af.handle_paternal_sisters()
        self.assertCountEqual([sister3], heirs)
        self.assertEqual(AshhaabulFuruudh.FARDH_ONE_HALF, part)

        self.assertCountEqual([sister1], af.handle_maternal_sisters()[af.ASHHABFARDH_KEY_HEIRS])

    def test_uncle(self):
        person = Person(0, "Person", Person.SEX_FEMALE)

        mom = Person(2, "Mom", Person.SEX_FEMALE)
        dad = Person(3, "Dad", Person.SEX_MALE)
        person.child_of(mom, dad)

        granddad0 = Person(4, "GrandDad0", Person.SEX_MALE)
        grandmom1 = Person(5, "GrandMom1", Person.SEX_FEMALE)
        granddad1 = Person(6, "GrandDad1", Person.SEX_MALE)
        grandmom2 = Person(7, "GrandMom2", Person.SEX_FEMALE)
        grandmom1.married(granddad0)
        grandmom1.married(granddad1)
        grandmom2.married(granddad1)

        dad.child_of(grandmom1, granddad1)

        brother1 = Person(11, "Brother1", Person.SEX_MALE)
        brother1.child_of(grandmom1, granddad0)
        brother2 = Person(12, "Brother2", Person.SEX_MALE)
        brother2.child_of(grandmom1, granddad1)
        brother3 = Person(13, "Brother3", Person.SEX_MALE)
        brother3.child_of(grandmom2, granddad1)

        af = AshhaabulFuruudh(person)
        heir, unused_part, unused_ashobah, _ = af.handle_father()
        self.assertEqual("Dad", heir.get_name())
        self.assertEqual("GrandDad1", heir.get_father().get_name())
        self.assertCountEqual([dad, brother2, brother3], heir.get_father().get_sons())
        self.assertCountEqual([dad, brother2, brother1], heir.get_mother().get_sons())
        self.assertEqual([brother2], af.handle_fathers_brothers()[af.ASHHABFARDH_KEY_HEIRS])
        self.assertCountEqual([brother3], af.handle_fathers_paternal_brothers()[af.ASHHABFARDH_KEY_HEIRS])

    def test_cousins(self):
        person = Person(0, "Person", Person.SEX_FEMALE)

        mom = Person(2, "Mom", Person.SEX_FEMALE)
        dad = Person(3, "Dad", Person.SEX_MALE)
        person.child_of(mom, dad)

        granddad0 = Person(4, "GrandDad0", Person.SEX_MALE)
        grandmom1 = Person(5, "GrandMom1", Person.SEX_FEMALE)
        granddad1 = Person(6, "GrandDad1", Person.SEX_MALE)
        grandmom2 = Person(7, "GrandMom2", Person.SEX_FEMALE)
        grandmom1.married(granddad0)
        grandmom1.married(granddad1)
        grandmom2.married(granddad1)

        dad.child_of(grandmom1, granddad1)

        brother1 = Person(11, "Brother1", Person.SEX_MALE)
        brother1.child_of(grandmom1, granddad0)
        brother2 = Person(12, "Brother2", Person.SEX_MALE)
        brother2.child_of(grandmom1, granddad1)
        brother3 = Person(13, "Brother3", Person.SEX_MALE)
        brother3.child_of(grandmom2, granddad1)

        wife1 = Person(15, "Brother1's wife", Person.SEX_FEMALE)
        wife2 = Person(16, "Brother2's wife", Person.SEX_FEMALE)
        wife3 = Person(17, "Brother3's wife", Person.SEX_FEMALE)

        cousin1 = Person(21, "Cousin1", Person.SEX_MALE)
        cousin1.child_of(wife1, brother1)
        cousin2 = Person(22, "Cousin2", Person.SEX_MALE)
        cousin2.child_of(wife2, brother2)
        cousin3 = Person(23, "Cousin3", Person.SEX_MALE)
        cousin3.child_of(wife3, brother3)

        af = AshhaabulFuruudh(person)
        self.assertCountEqual([cousin2], af.handle_son_of_fathers_brothers()[af.ASHHABFARDH_KEY_HEIRS])
        self.assertCountEqual([cousin3], af.handle_son_of_fathers_paternal_brothers()[af.ASHHABFARDH_KEY_HEIRS])

    def test_furu_female(self):
        person = Person(1, "Person", Person.SEX_FEMALE)
        husband = Person(2, "Husband", Person.SEX_MALE)

        son = Person(21, "Son", Person.SEX_MALE)
        son_wife = Person(22, "Son's Wife", Person.SEX_FEMALE)
        son.child_of(person, husband)

        daughter = Person(3, "Daughter", Person.SEX_FEMALE)
        daughter.child_of(person, husband)

        grandson = Person(23, "Grandson", Person.SEX_MALE)
        grandson_wife = Person(24, "Grandson's wife", Person.SEX_FEMALE)
        grandson.child_of(son_wife, son)
        grandson.died()

        granddaughter = Person(5, "Granddaughter", Person.SEX_FEMALE)
        granddaughter.child_of(son_wife, son)

        greatdaughter = Person(7, "Greatdaughter", Person.SEX_FEMALE)
        greatdaughter.child_of(grandson_wife, grandson)

        af = AshhaabulFuruudh(person)

        af.find()
        heirs, part, ashobah, _ = af.handle_daughters()
        self.assertEqual("Daughter", heirs[0].get_name())
        self.assertIsNone(part)
        self.assertTrue(ashobah)

        son.died()

        af.find()
        with self.assertRaises(UnableToFindLivingRelativeError):
            af.handle_sons()
        heirs, part, ashobah, _ = af.handle_daughters()
        self.assertEqual("Daughter", heirs[0].get_name())
        self.assertEqual(AshhaabulFuruudh.FARDH_ONE_HALF, part)
        self.assertFalse(ashobah)

        daughter2 = Person(24, "Daughter 2", Person.SEX_FEMALE)
        daughter2.child_of(person, husband)
        heirs, part, ashobah, _ = af.handle_daughters()
        self.assertCountEqual([daughter, daughter2], heirs)
        self.assertEqual(AshhaabulFuruudh.FARDH_TWO_THIRD, part)
        self.assertFalse(ashobah)

        daughter.died()
        daughter2.died()

        af.find()
        with self.assertRaises(UnableToFindLivingRelativeError):
            af.handle_daughters()
        heirs, part, unused_is_ashobah, _ = af.handle_granddaughters()
        self.assertTrue(heirs)
        self.assertEqual("Granddaughter", heirs[0].get_name())
        self.assertEqual(AshhaabulFuruudh.FARDH_ONE_HALF, part)

        granddaughter.died()
        heirs, part, unused_is_ashobah, _ = af.handle_granddaughters()
        self.assertTrue(heirs)
        self.assertEqual("Greatdaughter", heirs[0].get_name())
        self.assertEqual(AshhaabulFuruudh.FARDH_ONE_HALF, part)

        greatdaughter2 = Person(8, "Greatdaughter 2", Person.SEX_FEMALE)
        greatdaughter2.child_of(grandson_wife, grandson)

        heirs, part, unused_is_ashobah, _ = af.handle_granddaughters()
        self.assertTrue(heirs)
        self.assertCountEqual([greatdaughter, greatdaughter2], heirs)
        self.assertEqual(AshhaabulFuruudh.FARDH_TWO_THIRD, part)

        greatdaughter.died()
        greatdaughter2.died()
        with self.assertRaises(UnableToFindLivingRelativeError):
            af.handle_granddaughters()

    def test_ushul_female(self):
        person = Person(1, "Person", Person.SEX_FEMALE)

        mom = Person(5, "Mom", Person.SEX_FEMALE)
        dad = Person(4, "Dad", Person.SEX_MALE)
        person.child_of(mom, dad)

        grandmom = Person(6, "Grand Mom", Person.SEX_FEMALE)
        granddad = Person(7, "Grand Dad", Person.SEX_MALE)
        mom.child_of(grandmom, granddad)

        grandmom_fromdad = Person(8, "Grand Mom from Dad", Person.SEX_FEMALE)
        granddad_fromdad = Person(9, "Grand Dad from Dad", Person.SEX_MALE)
        dad.child_of(grandmom_fromdad, granddad_fromdad)

        greatmom = Person(6, "Great Mom", Person.SEX_FEMALE)
        greatdad = Person(7, "Great Dad", Person.SEX_MALE)
        grandmom.child_of(greatmom, greatdad)

        greatmom_fromgranddad = Person(10, "Greatmom from Granddad", Person.SEX_FEMALE)
        greatdad_fromgranddad = Person(11, "Greatdad from Granddad", Person.SEX_MALE)
        granddad_fromdad.child_of(greatmom_fromgranddad, greatdad_fromgranddad)

        af = AshhaabulFuruudh(person)
        af.find()
        heir, part, unused_is_ashobah, _ = af.handle_mother()
        self.assertEqual("Mom", heir.get_name())
        self.assertEqual(AshhaabulFuruudh.FARDH_ONE_THIRD, part)

        son = Person(12, "Son", Person.SEX_MALE)
        husband = Person(13, "Husband", Person.SEX_MALE)
        son.child_of(person, husband)

        af.find()
        heir, part, unused_is_ashobah, _ = af.handle_mother()
        self.assertEqual("Mom", heir.get_name())
        self.assertEqual(AshhaabulFuruudh.FARDH_ONE_SIXTH, part)

        son.died()
        husband.died()
        af.find()
        heir, part, unused_is_ashobah, _ = af.handle_mother()
        self.assertEqual(AshhaabulFuruudh.FARDH_ONE_THIRD, part)

        mom.died()
        with self.assertRaises(UnableToFindLivingRelativeError):
            af.handle_mother()
        heir, part, unused_is_ashobah, _ = af.handle_grandmothers()
        self.assertEqual("Grand Mom", heir.get_name())
        self.assertEqual(AshhaabulFuruudh.FARDH_ONE_SIXTH, part)

        heir, part, unused_is_ashobah, _ = af.handle_paternal_grandmothers()
        self.assertEqual("Grand Mom from Dad", heir.get_name())
        self.assertEqual(AshhaabulFuruudh.FARDH_ONE_SIXTH, part)

        grandmom.died()
        heir, part, unused_is_ashobah, _ = af.handle_grandmothers()
        self.assertEqual("Great Mom", heir.get_name())
        self.assertEqual(AshhaabulFuruudh.FARDH_ONE_SIXTH, part)

        grandmom_fromdad.died()
        heir, part, unused_is_ashobah, _ = af.handle_paternal_grandmothers()
        self.assertEqual("Greatmom from Granddad", heir.get_name())
        self.assertEqual(AshhaabulFuruudh.FARDH_ONE_SIXTH, part)

        greatmom.died()
        with self.assertRaises(UnableToFindLivingRelativeError):
            af.handle_grandmothers()

            greatmom_fromgranddad.died()
            self.assertFalse(af.handle_paternal_grandmothers())

    def test_get_wives(self):
        person = Person(1, "Person", Person.SEX_MALE)
        wife = Person(2, "Wife", Person.SEX_FEMALE)
        person.married(wife)

        af = AshhaabulFuruudh(person)
        heir, unused_part, unused_is_ashobah, _ = af.handle_wives()
        self.assertEqual("Wife", heir[0].get_name())

        wife.died()
        with self.assertRaises(UnableToFindLivingRelativeError):
            af.handle_wives()

    def test_soal_wife_noson(self):
        person = Person(1, "Person", Person.SEX_MALE)
        wife = Person(2, "Wife", Person.SEX_FEMALE)
        person.married(wife)

        af = AshhaabulFuruudh(person)
        heirs = af.find()
        self.assertCountEqual([AshhaabulFuruudh.TYPE_WIFE], heirs.keys())
        self.assertEqual(af.FARDH_ONE_FORTH, heirs[af.TYPE_WIFE][af.ASHHABFARDH_KEY_FARDH])


if __name__ == '__main__':
    unittest.main()
