from faraidh.person import Person
from faraidh.faraidh_settlement import FaraidhSettlement
from faraidh.ashhaabul_furuudh import AshhaabulFuruudh

import unittest


class TestFaraidhSetllement(unittest.TestCase):

    def test_single_fardh1(self):
        heirs = [Person(1, "person", Person.SEX_MALE)]
        afr = {
            1: (heirs, AshhaabulFuruudh.FARDH_ONE_HALF, None, [])
        }
        uf = FaraidhSettlement(afr)
        self.assertEqual(2, uf.tashil())

    def test_single_fardh2(self):
        heirs = [Person(1, "person", Person.SEX_MALE)]
        afr = {
            1: (heirs, AshhaabulFuruudh.FARDH_ONE_SIXTH, None, []),
            2: (heirs, AshhaabulFuruudh.FARDH_ONE_SIXTH, None, [])
        }
        uf = FaraidhSettlement(afr)
        self.assertEqual(6, uf.tashil())

    def test_single_fardh3(self):
        heirs = [Person(1, "person", Person.SEX_MALE)]
        afr = {
            1: (heirs, AshhaabulFuruudh.FARDH_ONE_THIRD, None, []),
            3: (heirs, AshhaabulFuruudh.FARDH_ONE_THIRD, None, []),
            2: (heirs, AshhaabulFuruudh.FARDH_TWO_THIRD, None, [])
        }
        uf = FaraidhSettlement(afr)
        self.assertEqual(3, uf.tashil())

    def test_double_fardh(self):
        heirs = [Person(1, "person", Person.SEX_MALE)]
        afr = {
            1: (heirs, AshhaabulFuruudh.FARDH_ONE_FORTH, None, []),
            2: (heirs, AshhaabulFuruudh.FARDH_ONE_FORTH, None, []),
            3: (heirs, AshhaabulFuruudh.FARDH_ONE_SIXTH, None, []),
            4: (heirs, AshhaabulFuruudh.FARDH_ONE_SIXTH, None, []),
            5: (heirs, AshhaabulFuruudh.FARDH_ONE_SIXTH, None, [])
        }
        uf = FaraidhSettlement(afr)
        self.assertEqual(12, uf.tashil())
        self.assertDictEqual({
            1: (3, 12),
            2: (3, 12),
            3: (2, 12),
            4: (2, 12),
            5: (2, 12)
        }, uf.share_ashhaabul_furuudh(12))

    def test_triple_fardh(self):
        heirs = [Person(1, "person", Person.SEX_MALE)]
        afr = {
            1: (heirs, AshhaabulFuruudh.FARDH_ONE_EIGHTH, None, []),
            2: (heirs, AshhaabulFuruudh.FARDH_ONE_EIGHTH, None, []),
            3: (heirs, AshhaabulFuruudh.FARDH_ONE_EIGHTH, None, []),
            4: (heirs, AshhaabulFuruudh.FARDH_ONE_EIGHTH, None, []),
            5: (heirs, AshhaabulFuruudh.FARDH_ONE_THIRD, None, []),
            6: (heirs, AshhaabulFuruudh.FARDH_ONE_SIXTH, None, [])
        }
        uf = FaraidhSettlement(afr)
        self.assertEqual(24, uf.tashil())
        self.assertDictEqual({
            1: (3, 24),
            2: (3, 24),
            3: (3, 24),
            4: (3, 24),
            5: (8, 24),
            6: (4, 24)
        }, uf.share_ashhaabul_furuudh(24))

    def test_with_ashobah(self):
        heirs = [Person(1, "person", Person.SEX_MALE)]
        afr = {
            1: (heirs, AshhaabulFuruudh.FARDH_ONE_FORTH, None, []),
            3: (heirs, AshhaabulFuruudh.FARDH_ONE_SIXTH, None, []),
            2: (heirs, None, AshhaabulFuruudh.FARDH_TWO_PART, [])
        }
        uf = FaraidhSettlement(afr)
        self.assertEqual(12, uf.tashil())
        self.assertDictEqual({
            1: (3, 12),
            3: (2, 12),
            uf.TYPE_ASHOBAH_UNSHARED: (7, 12)
        }, uf.share_ashhaabul_furuudh(12))
        self.assertDictEqual({
            2: (2, 2)
        }, uf.share_ashobah())

    def test_aul_6_to_7(self):
        heirs = [Person(1, "person", Person.SEX_MALE)]
        afr = {
            AshhaabulFuruudh.TYPE_HUSBAND: (heirs, AshhaabulFuruudh.FARDH_ONE_HALF, None, []),
            AshhaabulFuruudh.TYPE_SISTER: (heirs, AshhaabulFuruudh.FARDH_ONE_HALF, None, []),
            AshhaabulFuruudh.TYPE_GRANDMOTHER: (heirs, AshhaabulFuruudh.FARDH_ONE_SIXTH, None, [])
        }
        uf = FaraidhSettlement(afr)
        self.assertEqual(6, uf.tashil())
        self.assertDictEqual({
            AshhaabulFuruudh.TYPE_HUSBAND: (3, 7),
            AshhaabulFuruudh.TYPE_SISTER: (3, 7),
            AshhaabulFuruudh.TYPE_GRANDMOTHER: (1, 7)
        }, uf.share_ashhaabul_furuudh(6))

    def test_aul_6_to_8(self):
        heirs = [Person(1, "person", Person.SEX_MALE)]
        afr = {
            AshhaabulFuruudh.TYPE_HUSBAND: (heirs, AshhaabulFuruudh.FARDH_ONE_HALF, None, []),
            AshhaabulFuruudh.TYPE_SISTER: (heirs, AshhaabulFuruudh.FARDH_TWO_THIRD, None, []),
            AshhaabulFuruudh.TYPE_MOTHER: (heirs, AshhaabulFuruudh.FARDH_ONE_SIXTH, None, [])
        }
        uf = FaraidhSettlement(afr)
        self.assertEqual(6, uf.tashil())
        self.assertDictEqual({
            AshhaabulFuruudh.TYPE_HUSBAND: (3, 8),
            AshhaabulFuruudh.TYPE_SISTER: (4, 8),
            AshhaabulFuruudh.TYPE_MOTHER: (1, 8)
        }, uf.share_ashhaabul_furuudh(6))

    def test_aul_12_to_13(self):
        heirs = [Person(1, "person", Person.SEX_MALE)]
        afr = {
            AshhaabulFuruudh.TYPE_WIFE: (heirs, AshhaabulFuruudh.FARDH_ONE_FORTH, None, []),
            AshhaabulFuruudh.TYPE_MOTHER: (heirs, AshhaabulFuruudh.FARDH_ONE_SIXTH, None, []),
            AshhaabulFuruudh.TYPE_PATERNAL_SISTER: (heirs, AshhaabulFuruudh.FARDH_TWO_THIRD, None, [])
        }
        uf = FaraidhSettlement(afr)
        self.assertEqual(12, uf.tashil())
        self.assertDictEqual({
            AshhaabulFuruudh.TYPE_WIFE: (3, 13),
            AshhaabulFuruudh.TYPE_MOTHER: (2, 13),
            AshhaabulFuruudh.TYPE_PATERNAL_SISTER: (8, 13)
        }, uf.share_ashhaabul_furuudh(12))

    def test_aul_24_to_27(self):
        heirs = [Person(1, "person", Person.SEX_MALE)]
        afr = {
            AshhaabulFuruudh.TYPE_WIFE: (heirs, AshhaabulFuruudh.FARDH_ONE_EIGHTH, None, []),
            AshhaabulFuruudh.TYPE_GRANDFATHER: (heirs, AshhaabulFuruudh.FARDH_ONE_SIXTH, None, []),
            AshhaabulFuruudh.TYPE_MOTHER: (heirs, AshhaabulFuruudh.FARDH_ONE_SIXTH, None, []),
            AshhaabulFuruudh.TYPE_DAUGHTER: (heirs, AshhaabulFuruudh.FARDH_TWO_THIRD, None, [])
        }
        uf = FaraidhSettlement(afr)
        self.assertEqual(24, uf.tashil())
        self.assertDictEqual({
            AshhaabulFuruudh.TYPE_WIFE: (3, 27),
            AshhaabulFuruudh.TYPE_GRANDFATHER: (4, 27),
            AshhaabulFuruudh.TYPE_MOTHER: (4, 27),
            AshhaabulFuruudh.TYPE_DAUGHTER: (16, 27)
        }, uf.share_ashhaabul_furuudh(24))

    def test_rad(self):
        female = [Person(1, "person", Person.SEX_FEMALE)]
        afr = {
            AshhaabulFuruudh.TYPE_WIFE: (female, AshhaabulFuruudh.FARDH_ONE_FORTH, None, []),
            AshhaabulFuruudh.TYPE_GRANDMOTHER: (female, AshhaabulFuruudh.FARDH_ONE_SIXTH, None, []),
            AshhaabulFuruudh.TYPE_MATERNAL_SIBLING: (female, AshhaabulFuruudh.FARDH_ONE_THIRD, None, [])
        }
        uf = FaraidhSettlement(afr)
        self.assertEqual(12, uf.tashil())
        self.assertDictEqual({
            AshhaabulFuruudh.TYPE_WIFE: (3, 12),
            AshhaabulFuruudh.TYPE_GRANDMOTHER: (2, 12),
            AshhaabulFuruudh.TYPE_MATERNAL_SIBLING: (4, 12),
            FaraidhSettlement.TYPE_RAD_UNSHARED: (3, 12),
        }, uf.share_ashhaabul_furuudh(12))
        self.assertDictEqual({
            AshhaabulFuruudh.TYPE_GRANDMOTHER: (1, 3),
            AshhaabulFuruudh.TYPE_MATERNAL_SIBLING: (2, 3)
        }, uf.share_rad())


if __name__ == '__main__':
    unittest.main()
