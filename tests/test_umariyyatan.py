import unittest

from faraidh.ashhaabul_furuudh import AshhaabulFuruudh
from faraidh.person import Person
from faraidh.laws import IJTIMA


class TestUmariyyatan(unittest.TestCase):
    """@see http://media.isnet.org/kmi/islam/Waris/Umariyyatan.html"""

    def test_contoh_pertama(self):
        """
        Seorang istri wafat dan meninggalkan suami, ibu, dan ayah. Suami mendapat bagian setengah (1/2) dari seluruh
        harta warisan yang ada. Ibu mendapat sepertiga (1/3) dari sisa setelah diambil bagian suami. Kemudian ayah
        mendapat seluruh sisa yang ada.
        """

        # Seorang istri wafat dan
        person = Person(1, "Istri", Person.SEX_FEMALE)

        # meninggalkan suami,
        husband = Person(2, "Suami", Person.SEX_MALE)
        husband.married(person)

        # ibu, dan ayah
        mom = Person(3, "Ibu", Person.SEX_FEMALE)
        dad = Person(4, "Ayah", Person.SEX_MALE)
        person.child_of(mom, dad)

        af = AshhaabulFuruudh(person)
        heirs = af.find()
        self.assertCountEqual([
            AshhaabulFuruudh.TYPE_HUSBAND,
            AshhaabulFuruudh.TYPE_MOTHER,
            AshhaabulFuruudh.TYPE_FATHER
        ], heirs.keys())
        self.assertEqual(af.FARDH_ONE_HALF, heirs[af.TYPE_HUSBAND][af.ASHHABFARDH_KEY_FARDH])
        self.assertEqual(af.FARDH_ONE_SIXTH, heirs[af.TYPE_MOTHER][af.ASHHABFARDH_KEY_FARDH])
        self.assertIn(IJTIMA['umariyyatan'], heirs[af.TYPE_MOTHER][af.ASHHABFARDH_KEY_REASON])

        # ashobah (penghitungan bagian sisa)
        self.assertIsNone(heirs[af.TYPE_FATHER][af.ASHHABFARDH_KEY_FARDH])
        self.assertEqual(af.FARDH_TWO_PART, heirs[af.TYPE_FATHER][af.ASHHABFARDH_KEY_ASHOBAH])

    def test_contoh_kedua(self):
        """
        Seorang suami meninggal dunia dan ia meninggalkan istri, ibu, dan ayah. Istri mendapat bagian seperempat (1/4)
        dari seluruh harta peninggalan suaminya, sedangkan ibu mendapat bagian tiga per empat dari sisa setelah
        diambil hak istri. Sedangkan bagian ayah adalah sisa harta yang ada sebagai 'ashabah.
        """

        # Seorang suami wafat dan
        person = Person(1, "Suami", Person.SEX_MALE)

        # meninggalkan istri,
        wife = Person(2, "Istri", Person.SEX_FEMALE)
        wife.married(person)

        # ibu, dan ayah
        mom = Person(3, "Ibu", Person.SEX_FEMALE)
        dad = Person(4, "Ayah", Person.SEX_MALE)
        person.child_of(mom, dad)

        af = AshhaabulFuruudh(person)
        heirs = af.find()
        self.assertCountEqual([
            AshhaabulFuruudh.TYPE_WIFE,
            AshhaabulFuruudh.TYPE_MOTHER,
            AshhaabulFuruudh.TYPE_FATHER
        ], heirs.keys())
        self.assertEqual(af.FARDH_ONE_FORTH, heirs[af.TYPE_WIFE][af.ASHHABFARDH_KEY_FARDH])
        self.assertEqual(af.FARDH_ONE_FORTH, heirs[af.TYPE_MOTHER][af.ASHHABFARDH_KEY_FARDH])
        self.assertIn(IJTIMA['umariyyatan'], heirs[af.TYPE_MOTHER][af.ASHHABFARDH_KEY_REASON])

        # ayah ashobah
        self.assertIsNone(heirs[AshhaabulFuruudh.TYPE_FATHER][af.ASHHABFARDH_KEY_FARDH])
        self.assertEqual(af.FARDH_TWO_PART, heirs[AshhaabulFuruudh.TYPE_FATHER][af.ASHHABFARDH_KEY_ASHOBAH])


