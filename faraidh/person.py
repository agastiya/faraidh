from typing import List


class UnableToFindRelativeError(Exception):
    pass


class Person:

    SEX_MALE = 'm'
    SEX_FEMALE = 'f'

    RELATION_MOTHER = 'mother'
    RELATION_FATHER = 'father'
    RELATION_CHILDREN = 'children'
    RELATION_SPOUSES = 'spouses'

    @staticmethod
    def filter_by_sex(sex: str, people: List['Person']) -> List['Person']:
        return list(filter(lambda person: person.get_sex() == sex, people))

    def __str__(self) -> str:
        return self.__name

    def __init__(self, id: int, name: str, sex: str) -> None:
        self.__id = id
        self.__name = name
        self.__sex = sex
        self.__relations = {}
        self.__alive = True

    def get_key(self) -> int:
        return self.__id

    def get_name(self) -> str:
        return self.__name

    def get_sex(self) -> str:
        return self.__sex

    def relate_as(self, person: 'Person', relation: str) -> None:
        if relation not in self.__relations:
            self.__relations[relation] = set()
        self.__relations[relation].add(person)

    def get_relations(self, relation: str) -> List['Person']:
        if relation not in self.__relations:
            return []
        return list(self.__relations[relation])

    def get_relation(self, relation: str) -> 'Person':
        if relation not in self.__relations:
            raise UnableToFindRelativeError
        return list(self.__relations[relation])[0]

    def married(self, spouse) -> None:
        self.relate_as(spouse, self.RELATION_SPOUSES)
        spouse.relate_as(self, self.RELATION_SPOUSES)

    def child_of(self, mother: 'Person', father: 'Person') -> None:

        # Simplifying marriage status by always assume child is always legitimate.
        # In Islam, the law is differentiated for having children without marriage.
        # This condition will cause the child to be excluded in faraidh calculation.
        mother.married(father)

        self.relate_as(mother, self.RELATION_MOTHER)
        self.relate_as(father, self.RELATION_FATHER)
        mother.relate_as(self, self.RELATION_CHILDREN)
        father.relate_as(self, self.RELATION_CHILDREN)

    def died(self) -> None:
        self.__alive = False

    def is_alive(self) -> bool:
        return self.__alive

    # first-level relation getters

    def get_mother(self) -> 'Person':
        return self.get_relation(self.RELATION_MOTHER)

    def get_father(self) -> 'Person':
        return self.get_relation(self.RELATION_FATHER)

    def get_children(self) -> List['Person']:
        return self.get_relations(self.RELATION_CHILDREN)

    def get_sons(self) -> List['Person']:
        return self.filter_by_sex(self.SEX_MALE, self.get_children())

    def get_daughters(self) -> List['Person']:
        return self.filter_by_sex(self.SEX_FEMALE, self.get_children())

    def get_spouses(self) -> List['Person']:
        return self.get_relations(self.RELATION_SPOUSES)

    def get_husbands(self) -> List['Person']:
        return self.filter_by_sex(self.SEX_MALE, self.get_relations(self.RELATION_SPOUSES))

    def get_wives(self) -> List['Person']:
        return self.filter_by_sex(self.SEX_FEMALE, self.get_relations(self.RELATION_SPOUSES))

    # second-level relation getters

    def get_brothers(self) -> List['Person']:
        try:
            father = self.get_father()
            mother = self.get_mother()
        except UnableToFindRelativeError:
            return []

        paternal_brothers = father.get_sons()
        maternal_brothers = mother.get_sons()
        brothers = list(set(paternal_brothers).intersection(maternal_brothers))
        if self in brothers:
            brothers.remove(self)
        return brothers

    def get_sisters(self) -> List['Person']:
        try:
            father = self.get_father()
            mother = self.get_mother()
        except UnableToFindRelativeError:
            return []

        paternal_sisters = father.get_daughters()
        maternal_sisters = mother.get_daughters()
        sisters = list(set(paternal_sisters).intersection(maternal_sisters))
        if self in sisters:
            sisters.remove(self)
        return sisters

    def get_paternal_sisters(self) -> List['Person']:
        try:
            father = self.get_father()
        except UnableToFindRelativeError:
            return []

        sisters = father.get_daughters()
        if self in sisters:
            sisters.remove(self)

        return sisters

    def get_paternal_brothers(self) -> List['Person']:
        try:
            father = self.get_father()
            mother = self.get_mother()
        except UnableToFindRelativeError:
            return []

        brothers = father.get_sons()
        if self in brothers:
            brothers.remove(self)

        # exclude maternal brothers
        for brother in brothers:
            if brother.get_mother() == mother:
                brothers.remove(brother)

        return brothers

    def get_maternal_brothers(self) -> List['Person']:
        try:
            mother = self.get_mother()
        except UnableToFindRelativeError:
            return []

        brothers = mother.get_sons()
        if self in brothers:
            brothers.remove(self)

        return brothers

    def get_maternal_sisters(self) -> List['Person']:
        try:
            mother = self.get_mother()
        except UnableToFindRelativeError:
            return []

        sisters = mother.get_daughters()
        if self in sisters:
            sisters.remove(self)

        return sisters

