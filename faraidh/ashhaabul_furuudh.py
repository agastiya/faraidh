import itertools
from typing import Tuple, Dict, List, Union

from laws import QURAN, HADITS, IJTIMA
from .person import Person, UnableToFindRelativeError

Ashhab = List[Person]
Fardh = Tuple[int, int]
AshhabFardh = Tuple[Union[Person, Ashhab], Union[None, Fardh], Union[None, Fardh], List[str]]


class UnableToFindLivingRelativeError(Exception):
    pass


class AshhaabulFuruudh:
    """
    Ashabul furudh yaitu orang yang mendapatkan warisan (Ashhab/heir)
    sebesar kadar (Fardh/portion/share) berdasarkan aturan yang telah
    ditentukan (rules of descent and distribution) dalam kitabullah.

    @see https://rumaysho.com/2502-panduan-ringkas-ilmu-waris.html
    @see https://yufidia.com/perincian-pembagian-harta-waris/
    """

    # bagian harta yang didapat oleh seorang ahli waris
    # yang telah ditetapkan langsung oleh nash Al-Quran, As-Sunnah
    # atau ijma' ulama.
    FARDH_ONE_HALF = (1, 2)
    FARDH_ONE_FORTH = (1, 4)
    FARDH_ONE_EIGHTH = (1, 8)
    FARDH_TWO_THIRD = (2, 3)
    FARDH_ONE_THIRD = (1, 3)
    FARDH_ONE_SIXTH = (1, 6)
    FARDH_ONE_PART = (1, 0)
    FARDH_TWO_PART = (2, 0)

    # Relationship to the decedent
    TYPE_SON = 1
    TYPE_GRANDSON = 2
    TYPE_DAUGHTER = 3
    TYPE_GRANDDAUGHTER = 4
    TYPE_WIFE = 5
    TYPE_HUSBAND = 6
    TYPE_FATHER = 7
    TYPE_GRANDFATHER = 8
    TYPE_MOTHER = 9
    TYPE_PATERNAL_GRANDMOTHER = 10
    TYPE_GRANDMOTHER = 11
    TYPE_MATERNAL_SIBLING = 12
    TYPE_BROTHER = 13
    TYPE_SISTER = 14
    TYPE_PATERNAL_BROTHER = 15
    TYPE_PATERNAL_SISTER = 16
    TYPE_SON_OF_BROTHER = 17
    TYPE_SON_OF_PATERNAL_BROTHER = 18
    TYPE_FATHERS_BROTHER = 19
    TYPE_FATHERS_PATERNAL_BROTHER = 20
    TYPE_SON_OF_FATHERS_BROTHER = 21
    TYPE_SON_OF_FATHERS_PATERNAL_BROTHER = 22

    ASHHABFARDH_KEY_HEIRS = 0
    ASHHABFARDH_KEY_FARDH = 1
    ASHHABFARDH_KEY_ASHOBAH = 2
    ASHHABFARDH_KEY_REASON = 3

    # Kerabat laki-laki yang berhak menerima warisan ada lima belas orang
    # (10 group), termasuk bekas budak laki2.
    # Selain yang disebut di atas termasuk dzawil arham, seperti
    # - paman dari pihak ibu,
    # - anak laki-laki saudara seibu,
    # - paman seibu, dan
    # - anak laki-laki paman seibu dan semisalnya
    # tidak mendapat harta waris. (Lihat Muhtashar Fiqhul Islami, hlm. 775-776)
    __MALE = {
        'get_sons',
        'get_grandsons',
        'get_father',
        'get_grandfathers',
        # saudara laki-laki
        'get_brothers', 'get_paternal_brothers', 'get_maternal_brothers',
        # anak laki-laki dari saudara laki-laki (keponakan) walaupun jauh (seperti anak dari keponakan)
        'get_son_of_brothers', 'get_son_of_paternal_brothers',
        # paman
        'get_fathers_brothers', 'get_fathers_paternal_brothers',
        # anak laki-laki dari paman (sepupu) walaupun jauh
        'get_son_of_fathers_brothers', 'get_son_of_fathers_paternal_brothers',
        'get_husbands'
    }

    # ahli waris dari perempuan ada 11 orang (7 group), termasuk bekas budak perempuan
    # Semua keluarga wanita selain ahli waris sebelas ini, seperti
    # bibi dan seterusnya dinamakan dzawil arham, tidak mendapat harta waris.
    # (Lihat muhtashar Fiqhul Islami, hlm. 776)
    __FEMALE = {
        'get_daughters',
        'get_granddaughters',
        'get_mother',
        # nenek dan seterusnya ke atas
        'get_grandmothers',  'get_paternal_grandmothers',
        # saudara perempuan
        'get_sisters', 'get_paternal_sisters', 'get_maternal_sisters',
        'get_wives'
    }

    # Hak waris yang tidak bisa gugur meskipun semua ahli waris ada, mereka tetap akan mendapat bagian harta warisan
    # meskipun dapat berkurang. Mereka adalah ahli waris dekat yang disebut al-aqrabun.
    __AQRABUN = {
        'get_husbands', 'get_wives',
        'get_father', 'get_mother',
        'get_sons', 'get_daughters'
    }

    # ‘Ashobah yaitu orang yang mendapatkan warisan dari kelebihan harta setelah diserahkan pada ashabul furudh.
    # diurutkan dari yang paling dekat
    __ASHOBAH = {
        'get_sons',
        'get_grandsons',
        'get_father',
        'get_grandfathers',
        'get_brothers',
        'get_paternal_brothers',
        'get_son_of_brothers',
        'get_son_of_paternal_brothers',
        'get_fathers_brothers',
        'get_son_of_fathers_brothers'
    }

    # based on Tabel Waris Ust. Sarwat rearranged
    # - supaya angka besar selalu require yang lebih kecil, dan
    # - sesama grup berdekatan
    HEIR_HANDLERS = {
        # Group A (Children)
        1:  {'active': True, 'function_name': 'handle_sons'},
        2:  {'active': True, 'function_name': 'handle_grandsons'},
        3:  {'active': True, 'function_name': 'handle_daughters'},
        4:  {'active': True, 'function_name': 'handle_granddaughters'},
        # Group B (Spouses)
        5:  {'active': True, 'function_name': 'handle_wives'},
        6:  {'active': True, 'function_name': 'handle_husbands'},
        # Group C (Parents)
        7:  {'active': True, 'function_name': 'handle_father'},
        8:  {'active': True, 'function_name': 'handle_grandfathers'},
        # Group D (Siblings)
        # "... Jika seseorang mati baik laki-laki maupun perempuan
        # yang tidak meninggalkan ayah dan tidak meninggalkan anak,
        # tetapi mempunyai seorang saudara laki-laki (seibu saja) atau
        # seorang saudara perempuan (seibu saja), ...
        # (an-Nisa': 12)
        12: {'active': True, 'function_name': 'handle_maternal_brothers|handle_maternal_sisters'},
        13: {'active': True, 'function_name': 'handle_brothers'},
        14: {'active': True, 'function_name': 'handle_sisters'},
        15: {'active': True, 'function_name': 'handle_paternal_brothers'},
        16: {'active': True, 'function_name': 'handle_paternal_sisters'},
        # Group C-2 (Moms)
        9:  {'active': True, 'function_name': 'handle_mother'},
        10: {'active': True, 'function_name': 'handle_paternal_grandmothers'},
        11: {'active': True, 'function_name': 'handle_grandmothers'},
        # Group E (Keponakan)
        17: {'active': True, 'function_name': 'handle_son_of_brothers'},
        18: {'active': True, 'function_name': 'handle_son_of_paternal_brothers'},
        # Group F (Paman dan Sepupu)
        19: {'active': True, 'function_name': 'handle_fathers_brothers'},
        20: {'active': True, 'function_name': 'handle_fathers_paternal_brothers'},
        21: {'active': True, 'function_name': 'handle_son_of_fathers_brothers'},
        22: {'active': True, 'function_name': 'handle_son_of_fathers_paternal_brothers'}
    }

    @staticmethod
    def filter_alive(people: Ashhab) -> Ashhab:
        if not people:
            raise UnableToFindLivingRelativeError
        living = list(filter(lambda p: p.is_alive(), people))
        if not living:
            raise UnableToFindLivingRelativeError
        return living

    @staticmethod
    def get_sons_of(people: Ashhab) -> Ashhab:
        return list(itertools.chain(*map(lambda person: person.get_sons(), people)))

    @staticmethod
    def get_daughters_of(people: Ashhab) -> Ashhab:
        return list(itertools.chain(*map(lambda person: person.get_daughters(), people)))

    def __init__(self, decedent: Person) -> None:
        self._decedent = decedent
        self._reset()

    def _reset(self) -> None:
        self._heirs_order = [*self.HEIR_HANDLERS]
        self._heirs_found = {}

    def _mahjub_hirman(self, hajb: List[int]) -> None:
        for relation in hajb:
            if relation in self._heirs_order:
                self._heirs_order.remove(relation)

    def _merge_ashhab_fardh(self, base: AshhabFardh, p: AshhabFardh) -> AshhabFardh:
        if not p:
            return base
        heirs = p[self.ASHHABFARDH_KEY_HEIRS]
        if isinstance(heirs, list):
            heirs = list(itertools.chain(base[self.ASHHABFARDH_KEY_HEIRS], heirs))
        return (
            heirs,
            p[self.ASHHABFARDH_KEY_FARDH],
            p[self.ASHHABFARDH_KEY_ASHOBAH],
            list(itertools.chain(base[self.ASHHABFARDH_KEY_REASON], p[self.ASHHABFARDH_KEY_REASON]))
        )

    def find(self) -> Dict[int, AshhabFardh]:
        base = [], None, None, []
        self._reset()
        for relation in self._heirs_order:
            if not self.HEIR_HANDLERS[relation]['active']:
                continue

            p = base
            for function_name in self.HEIR_HANDLERS[relation]['function_name'].split('|'):
                try:
                    p = self._merge_ashhab_fardh(p, getattr(self, function_name)())
                except UnableToFindLivingRelativeError:
                    pass
            if p[self.ASHHABFARDH_KEY_HEIRS]:
                self._heirs_found[relation] = p
        return self._heirs_found

    # Relation handlers
    # All results from functions below should be fetched using the find() method above

    # Ayah
    def handle_father(self) -> AshhabFardh:
        try:
            father = self._decedent.get_father()
        except UnableToFindRelativeError:
            raise UnableToFindLivingRelativeError

        if not father.is_alive():
            raise UnableToFindLivingRelativeError

        self._mahjub_hirman([
            self.TYPE_GRANDFATHER,
            self.TYPE_PATERNAL_GRANDMOTHER,
            self.TYPE_MATERNAL_SIBLING,
            self.TYPE_BROTHER,
            self.TYPE_SISTER,
            self.TYPE_PATERNAL_BROTHER,
            self.TYPE_PATERNAL_SISTER,
            self.TYPE_SON_OF_BROTHER,
            self.TYPE_SON_OF_PATERNAL_BROTHER,
            self.TYPE_FATHERS_BROTHER,
            self.TYPE_FATHERS_PATERNAL_BROTHER,
            self.TYPE_SON_OF_FATHERS_BROTHER,
            self.TYPE_SON_OF_FATHERS_PATERNAL_BROTHER
        ])

        # with son as ashobah:1, father is a nuqshan
        if self.TYPE_SON in self._heirs_found:
            return father, self.FARDH_ONE_SIXTH, None, [QURAN['an-Nisa:11:5'], IJTIMA['hijab-nuqshan:2a'], IJTIMA['hijab-nuqshan:2b']]
        if self.TYPE_GRANDSON in self._heirs_found:
            # with or without granddaughter
            return father, self.FARDH_ONE_SIXTH, None, [QURAN['an-Nisa:11:5'], IJTIMA['hijab-nuqshan:2a'], IJTIMA['hijab-nuqshan:2b']]

        # no son as ashobah:1, father become ashobah:2
        # FIXME: bagaimana jika ada pendapat bahwa bapak tidak terhijab nuqshon oleh anak perempuan,
        #        apakah berarti fardh 1/3?
        if self.TYPE_DAUGHTER in self._heirs_found:
            return father, self.FARDH_ONE_SIXTH, self.FARDH_TWO_PART, [QURAN['an-Nisa:11:5'], HADITS['bukhari:1'], IJTIMA['hijab-nuqshan:2b'], IJTIMA['ashobah-bin-nafs:2']]
        if self.TYPE_GRANDDAUGHTER in self._heirs_found:
            return father, self.FARDH_ONE_SIXTH, self.FARDH_TWO_PART, [QURAN['an-Nisa:11:5'], HADITS['bukhari:1'], IJTIMA['hijab-nuqshan:2b'], IJTIMA['ashobah-bin-nafs:2']]

        return father, None, self.FARDH_TWO_PART, [QURAN['an-Nisa:11:6'], IJTIMA['ashobah-bin-nafs:2']]

    # Ibu
    def handle_mother(self) -> AshhabFardh:
        try:
            mother = self._decedent.get_mother()
        except UnableToFindRelativeError:
            raise UnableToFindLivingRelativeError

        if not mother.is_alive():
            raise UnableToFindLivingRelativeError

        self._mahjub_hirman([
            self.TYPE_PATERNAL_GRANDMOTHER,
            self.TYPE_GRANDMOTHER
        ])

        if self.TYPE_GRANDDAUGHTER in self._heirs_found:
            return mother, self.FARDH_ONE_SIXTH, None, [QURAN['an-Nisa:11:5'], IJTIMA['hijab-nuqshan:3b']]
        if self.TYPE_DAUGHTER in self._heirs_found:
            return mother, self.FARDH_ONE_SIXTH, None, [QURAN['an-Nisa:11:5'], IJTIMA['hijab-nuqshan:3b'], IJTIMA['hijab-nuqshan:3c']]
        if self.TYPE_SON in self._heirs_found:
            return mother, self.FARDH_ONE_SIXTH, None, [QURAN['an-Nisa:11:5'], IJTIMA['hijab-nuqshan:3b'], IJTIMA['hijab-nuqshan:3c'], IJTIMA['hijab-nuqshan:3a']]
        if self.TYPE_GRANDSON in self._heirs_found:
            return mother, self.FARDH_ONE_SIXTH, None, [QURAN['an-Nisa:11:5'], IJTIMA['hijab-nuqshan:3b'], IJTIMA['hijab-nuqshan:3c'], IJTIMA['hijab-nuqshan:3a']]

        num_sibblings = 0
        for sibbling_type in [self.TYPE_MATERNAL_SIBLING, self.TYPE_BROTHER, self.TYPE_SISTER, self.TYPE_PATERNAL_BROTHER, self.TYPE_PATERNAL_SISTER]:
            if sibbling_type not in self._heirs_found:
                continue
            num_sibblings += len(self._heirs_found[sibbling_type][self.ASHHABFARDH_KEY_HEIRS])
            if num_sibblings >= 2:
                return mother, self.FARDH_ONE_SIXTH, None, [QURAN['an-Nisa:11:7'], IJTIMA['hijab-nuqshan:4']]

        if self.TYPE_FATHER in self._heirs_found:
            if self.TYPE_WIFE in self._heirs_found:
                # 1/3 dari sisa (3/4) setelah dikurangi bagian isteri (1/4),
                # pada hakikatnya bagian ibu adalah seperempat (1/4),
                return mother, self.FARDH_ONE_FORTH, None, [QURAN['an-Nisa:11:6'], IJTIMA['umariyyatan']]
            if self.TYPE_HUSBAND in self._heirs_found:
                # 1/3 dari sisa (3/6) setelah dikurangi bagian suami (1/2),
                # pada hakikatnya bagian ibu adalah seperenam (1/6),
                return mother, self.FARDH_ONE_SIXTH, None, [QURAN['an-Nisa:11:6'], IJTIMA['umariyyatan']]

        return mother, self.FARDH_ONE_THIRD, None, [QURAN['an-Nisa:11:6']]

    # Kakek (ayahnya ayah) dan seterusnya ke atas
    def handle_grandfathers(self) -> AshhabFardh:
        try:
            father = self._decedent.get_father()
            while True:
                grandfather = father.get_father()
                if grandfather.is_alive():
                    break
                father = grandfather
        except UnableToFindRelativeError:
            raise UnableToFindLivingRelativeError

        self._mahjub_hirman([
            self.TYPE_MATERNAL_SIBLING,
            self.TYPE_BROTHER,
            self.TYPE_SISTER,
            self.TYPE_PATERNAL_BROTHER,
            self.TYPE_PATERNAL_SISTER,
            self.TYPE_SON_OF_BROTHER,
            self.TYPE_SON_OF_PATERNAL_BROTHER,
            self.TYPE_FATHERS_BROTHER,
            self.TYPE_FATHERS_PATERNAL_BROTHER,
            self.TYPE_SON_OF_FATHERS_BROTHER,
            self.TYPE_SON_OF_FATHERS_PATERNAL_BROTHER
        ])

        # with son/grandson as ashobah:1, grandfather become a nuqshan:2
        if self.TYPE_SON in self._heirs_found:
            # with or without daughter
            return grandfather, self.FARDH_ONE_SIXTH, None, [QURAN['an-Nisa:11:5'], IJTIMA['hijab-nuqshan:2a'], IJTIMA['hijab-nuqshan:2b']]
        if self.TYPE_GRANDSON in self._heirs_found:
            # with or without granddaughter
            return grandfather, self.FARDH_ONE_SIXTH, None, [QURAN['an-Nisa:11:5'], IJTIMA['hijab-nuqshan:2a'], IJTIMA['hijab-nuqshan:2b']]

        # no son as ashobah:1, grandfather become ashobah:2
        # FIXME: bagaimana jika ada pendapat bahwa bapak tidak terhijab nuqshon oleh anak perempuan,
        #        apakah berarti fardh 1/3?
        if self.TYPE_DAUGHTER in self._heirs_found:
            return grandfather, self.FARDH_ONE_SIXTH, self.FARDH_TWO_PART, [QURAN['an-Nisa:11:5'], HADITS['bukhari:1'], IJTIMA['hijab-nuqshan:2b'], IJTIMA['ashobah-bin-nafs:2']]
        if self.TYPE_GRANDDAUGHTER in self._heirs_found:
            return grandfather, self.FARDH_ONE_SIXTH, self.FARDH_TWO_PART, [QURAN['an-Nisa:11:5'], HADITS['bukhari:1'], IJTIMA['hijab-nuqshan:2b'], IJTIMA['ashobah-bin-nafs:2']]

        return grandfather, None, self.FARDH_TWO_PART, [QURAN['an-Nisa:11:6'], HADITS['bukhari:1'], IJTIMA['ashobah-bin-nafs:2']]

    # Nenek (ibunya ibu) dan seterusnya ke atas
    def handle_grandmothers(self) -> AshhabFardh:
        try:
            mother = self._decedent.get_mother()
            while True:
                grandmother = mother.get_mother()
                if grandmother.is_alive():
                    break
                mother = grandmother
        except UnableToFindRelativeError:
            raise UnableToFindLivingRelativeError

        return grandmother, self.FARDH_ONE_SIXTH, None, [QURAN['an-Nisa:11:5']]

    # Nenek (ibunya bapak) dan seterusnya ke atas
    def handle_paternal_grandmothers(self) -> AshhabFardh:
        try:
            father = self._decedent.get_father()
            while True:
                grandmother = father.get_mother()
                if grandmother.is_alive():
                    break
                father = father.get_father()
        except UnableToFindRelativeError:
            raise UnableToFindLivingRelativeError

        return grandmother, self.FARDH_ONE_SIXTH, None, [QURAN['an-Nisa:11:5']]

    # anak laki-laki
    def handle_sons(self) -> AshhabFardh:
        heirs = self.filter_alive(self._decedent.get_sons())

        self._mahjub_hirman([
            self.TYPE_GRANDSON,
            self.TYPE_GRANDDAUGHTER,
            self.TYPE_MATERNAL_SIBLING,
            self.TYPE_BROTHER,
            self.TYPE_SISTER,
            self.TYPE_PATERNAL_BROTHER,
            self.TYPE_PATERNAL_SISTER,
            self.TYPE_SON_OF_BROTHER,
            self.TYPE_SON_OF_PATERNAL_BROTHER,
            self.TYPE_FATHERS_BROTHER,
            self.TYPE_FATHERS_PATERNAL_BROTHER,
            self.TYPE_SON_OF_FATHERS_BROTHER,
            self.TYPE_SON_OF_FATHERS_PATERNAL_BROTHER
        ])

        return heirs, None, self.FARDH_TWO_PART, [QURAN['an-Nisa:11:2'], IJTIMA['ashobah-bin-nafs:1']]

    # anak perempuan
    def handle_daughters(self) -> AshhabFardh:
        heir = self.filter_alive(self._decedent.get_daughters())

        if self.TYPE_SON in self._heirs_found:
            self._mahjub_hirman([
                self.TYPE_GRANDDAUGHTER,
                self.TYPE_MATERNAL_SIBLING
            ])
            return heir, None, self.FARDH_ONE_PART, [QURAN['an-Nisa:176:5'], QURAN['an-Nisa:11:2'], IJTIMA['ashobah-bi-ghairihi:1']]

        if len(heir) == 1:
            self._mahjub_hirman([
                self.TYPE_MATERNAL_SIBLING
            ])
            return heir, self.FARDH_ONE_HALF, None, [QURAN['an-Nisa:11:4']]
        else:
            self._mahjub_hirman([
                self.TYPE_GRANDDAUGHTER,
                self.TYPE_MATERNAL_SIBLING
            ])
            return heir, self.FARDH_TWO_THIRD, None, [QURAN['an-Nisa:11:3']]

    # Cucu laki-laki (dari anak laki) dan seterusnya ke bawah
    def handle_grandsons(self) -> AshhabFardh:
        living = []
        sons = self._decedent.get_sons()
        while sons and not living:
            grandsons = self.get_sons_of(sons)
            try:
                living = self.filter_alive(grandsons)
            except UnableToFindLivingRelativeError:
                sons = grandsons

        if not living:
            raise UnableToFindLivingRelativeError

        self._mahjub_hirman([
            self.TYPE_MATERNAL_SIBLING,
            self.TYPE_BROTHER,
            self.TYPE_SISTER,
            self.TYPE_PATERNAL_BROTHER,
            self.TYPE_PATERNAL_SISTER,
            self.TYPE_SON_OF_BROTHER,
            self.TYPE_SON_OF_PATERNAL_BROTHER,
            self.TYPE_FATHERS_BROTHER,
            self.TYPE_FATHERS_PATERNAL_BROTHER,
            self.TYPE_SON_OF_FATHERS_BROTHER,
            self.TYPE_SON_OF_FATHERS_PATERNAL_BROTHER
        ])

        return living, None, self.FARDH_TWO_PART, [QURAN['an-Nisa:11:2'], IJTIMA['ashobah-bin-nafs:1']]

    # anak perempuan dari anak laki-laki (cucu perempuan) dan seterusnya ke bawah
    def handle_granddaughters(self) -> AshhabFardh:
        living = []
        sons = self._decedent.get_sons()
        while sons and not living:
            granddaughters = self.get_daughters_of(sons)
            try:
                living = self.filter_alive(granddaughters)
            except UnableToFindLivingRelativeError:
                sons = self.get_sons_of(sons)

        if not living:
            raise UnableToFindLivingRelativeError

        self._mahjub_hirman([
            self.TYPE_MATERNAL_SIBLING
        ])

        if self.TYPE_GRANDSON in self._heirs_found:
            return living, None, self.FARDH_ONE_PART, [QURAN['an-Nisa:176:5'], IJTIMA['ashobah-bi-ghairihi:2']]

        # FIXME: hajb nuqshan
        if self.TYPE_DAUGHTER in self._heirs_found:
            return living, self.FARDH_ONE_SIXTH, None, [HADITS['ibn-masud:1:3']]

        if len(living) == 1:
            return living, self.FARDH_ONE_HALF, None, [QURAN['an-Nisa:11:4']]
        else:
            return living, self.FARDH_TWO_THIRD, None, [QURAN['an-Nisa:11:3']]

    # Suami
    def handle_husbands(self) -> AshhabFardh:
        heir = self.filter_alive(self._decedent.get_husbands())

        # hajb nuqshan
        if self.TYPE_SON in self._heirs_found:
            return heir, self.FARDH_ONE_FORTH, None, [QURAN['an-Nisa:12:2'], IJTIMA['hijab-nuqshan:1']]
        elif self.TYPE_DAUGHTER in self._heirs_found:
            return heir, self.FARDH_ONE_FORTH, None, [QURAN['an-Nisa:12:2']]
        elif self.TYPE_GRANDSON in self._heirs_found:
            return heir, self.FARDH_ONE_FORTH, None, [QURAN['an-Nisa:12:2'], IJTIMA['hijab-nuqshan:1']]
        elif self.TYPE_GRANDDAUGHTER in self._heirs_found:
            return heir, self.FARDH_ONE_FORTH, None, [QURAN['an-Nisa:12:2']]
        else:
            return heir, self.FARDH_ONE_HALF, None, [QURAN['an-Nisa:12:1']]

    # Istri
    def handle_wives(self) -> AshhabFardh:
        heir = self.filter_alive(self._decedent.get_wives())

        # hajb nuqshan
        if self.TYPE_SON in self._heirs_found:
            return heir, self.FARDH_ONE_EIGHTH, None, [QURAN['an-Nisa:12:4'], IJTIMA['hijab-nuqshan:1']]
        elif self.TYPE_DAUGHTER in self._heirs_found:
            return heir, self.FARDH_ONE_EIGHTH, None, [QURAN['an-Nisa:12:4']]
        elif self.TYPE_GRANDSON in self._heirs_found:
            return heir, self.FARDH_ONE_EIGHTH, None, [QURAN['an-Nisa:12:4'], IJTIMA['hijab-nuqshan:1']]
        elif self.TYPE_GRANDDAUGHTER in self._heirs_found:
            return heir, self.FARDH_ONE_EIGHTH, None, [QURAN['an-Nisa:12:4']]
        else:
            return heir, self.FARDH_ONE_FORTH, None, [QURAN['an-Nisa:12:3']]

    # Saudara laki-laki seayah-ibu
    def handle_brothers(self) -> AshhabFardh:
        try:
            brothers = self._decedent.get_brothers()
            heirs = self.filter_alive(brothers)
        except UnableToFindRelativeError:
            raise UnableToFindLivingRelativeError

        self._mahjub_hirman([
            self.TYPE_PATERNAL_BROTHER,
            self.TYPE_PATERNAL_SISTER,
            self.TYPE_SON_OF_BROTHER,
            self.TYPE_SON_OF_PATERNAL_BROTHER,
            self.TYPE_FATHERS_BROTHER,
            self.TYPE_FATHERS_PATERNAL_BROTHER,
            self.TYPE_SON_OF_FATHERS_BROTHER,
            self.TYPE_SON_OF_FATHERS_PATERNAL_BROTHER
        ])

        return heirs, None, self.FARDH_TWO_PART, [QURAN['an-Nisa:11:2'], IJTIMA['ashobah-bin-nafs:3']]

    # Saudara laki-laki sebapak
    def handle_paternal_brothers(self) -> AshhabFardh:
        try:
            brothers = self._decedent.get_paternal_brothers()
            heirs = self.filter_alive(brothers)
        except UnableToFindRelativeError:
            raise UnableToFindLivingRelativeError

        self._mahjub_hirman([
            self.TYPE_SON_OF_BROTHER,
            self.TYPE_SON_OF_PATERNAL_BROTHER,
            self.TYPE_FATHERS_BROTHER,
            self.TYPE_FATHERS_PATERNAL_BROTHER,
            self.TYPE_SON_OF_FATHERS_BROTHER,
            self.TYPE_SON_OF_FATHERS_PATERNAL_BROTHER
        ])

        return heirs, None, self.FARDH_TWO_PART, [QURAN['an-Nisa:11:2'], IJTIMA['ashobah-bin-nafs:3']]

    # Saudara laki-laki seibu
    def handle_maternal_brothers(self) -> AshhabFardh:
        try:
            brothers = self._decedent.get_maternal_brothers()
            dad = self._decedent.get_father()

            # eliminate saudara laki-laki seayah
            for brother in brothers:
                if brother.get_father() == dad:
                    brothers.remove(brother)

            heirs = self.filter_alive(brothers)

        except UnableToFindRelativeError:
            raise UnableToFindLivingRelativeError

        if len(heirs) == 1:
            return heirs, self.FARDH_ONE_SIXTH, None, [QURAN['an-Nisa:12:5'], QURAN['an-Nisa:12:6']]
        else:
            return heirs, self.FARDH_ONE_THIRD, None, [QURAN['an-Nisa:12:5'], QURAN['an-Nisa:12:7']]

    # Saudara perempuan seibu
    def handle_maternal_sisters(self) -> AshhabFardh:
        try:
            sisters = self._decedent.get_maternal_sisters()
            dad = self._decedent.get_father()

            # eliminate saudara perempuan seayah
            for sister in sisters:
                if sister.get_father() == dad:
                    sisters.remove(sister)

            heirs = self.filter_alive(sisters)

        except UnableToFindRelativeError:
            raise UnableToFindLivingRelativeError

        if len(heirs) == 1:
            return heirs, self.FARDH_ONE_SIXTH, None, [QURAN['an-Nisa:12:5'], QURAN['an-Nisa:12:6']]
        else:
            return heirs, self.FARDH_ONE_THIRD, None, [QURAN['an-Nisa:12:5'], QURAN['an-Nisa:12:7']]

    def handle_sisters(self) -> AshhabFardh:
        try:
            sisters = self._decedent.get_sisters()
            heirs = self.filter_alive(sisters)
        except UnableToFindRelativeError:
            raise UnableToFindLivingRelativeError

        is_ashobah = False
        reason = []
        if self.TYPE_BROTHER in self._heirs_found:
            is_ashobah = True
            reason.append(QURAN['an-Nisa:176:5'])
            reason.append(IJTIMA['ashobah-bi-ghairihi:3'])
        elif self.TYPE_DAUGHTER in self._heirs_found and self.TYPE_SON not in self._heirs_found:
            is_ashobah = True
            reason.append(HADITS['ibn-masud:1:4'])
            reason.append(IJTIMA['ashobah-maal-ghair'])
        elif self.TYPE_GRANDDAUGHTER in self._heirs_found and self.TYPE_GRANDSON not in self._heirs_found:
            is_ashobah = True
            reason.append(HADITS['ibn-masud:1:4'])
            reason.append(IJTIMA['ashobah-maal-ghair'])

        if is_ashobah:
            self._mahjub_hirman([
                self.TYPE_PATERNAL_BROTHER,
                self.TYPE_PATERNAL_SISTER,
                self.TYPE_SON_OF_BROTHER,
                self.TYPE_SON_OF_PATERNAL_BROTHER,
                self.TYPE_FATHERS_BROTHER,
                self.TYPE_FATHERS_PATERNAL_BROTHER,
                self.TYPE_SON_OF_FATHERS_BROTHER,
                self.TYPE_SON_OF_FATHERS_PATERNAL_BROTHER
            ])
            return heirs, None, self.FARDH_ONE_PART, reason

        if len(heirs) == 1:
            return heirs, self.FARDH_ONE_HALF, None, [QURAN['an-Nisa:176:2']]

        self._mahjub_hirman([
            self.TYPE_PATERNAL_SISTER
        ])
        return heirs, self.FARDH_TWO_THIRD, None, [QURAN['an-Nisa:176:4']]

    # Saudari seayah
    def handle_paternal_sisters(self) -> AshhabFardh:
        try:
            sisters = self._decedent.get_paternal_sisters()
            mom = self._decedent.get_mother()

            # eliminate saudari seibu
            for sister in sisters:
                if sister.get_mother() == mom:
                    sisters.remove(sister)

            heirs = self.filter_alive(sisters)

        except UnableToFindRelativeError:
            raise UnableToFindLivingRelativeError

        is_ashobah = False
        reason = []
        if self.TYPE_PATERNAL_BROTHER in self._heirs_found:
            is_ashobah = True
            reason.append(QURAN['an-Nisa:176:5'])
            reason.append(IJTIMA['ashobah-bi-ghairihi:4'])
        elif self.TYPE_DAUGHTER in self._heirs_found and self.TYPE_SON not in self._heirs_found:
            is_ashobah = True
            reason.append(HADITS['ibn-masud:1:4'])
            reason.append(IJTIMA['ashobah-maal-ghair'])
        elif self.TYPE_GRANDDAUGHTER in self._heirs_found and self.TYPE_GRANDSON not in self._heirs_found:
            is_ashobah = True
            reason.append(HADITS['ibn-masud:1:4'])
            reason.append(IJTIMA['ashobah-maal-ghair'])

        if is_ashobah:
            self._mahjub_hirman([
                self.TYPE_SON_OF_BROTHER,
                self.TYPE_SON_OF_PATERNAL_BROTHER,
                self.TYPE_FATHERS_BROTHER,
                self.TYPE_FATHERS_PATERNAL_BROTHER,
                self.TYPE_SON_OF_FATHERS_BROTHER,
                self.TYPE_SON_OF_FATHERS_PATERNAL_BROTHER
            ])
            return heirs, None, self.FARDH_ONE_PART, reason

        # FIXME: hajb nuqshan
        if self.TYPE_SISTER in self._heirs_found and \
                len(self._heirs_found[self.TYPE_SISTER][self.ASHHABFARDH_KEY_HEIRS]) == 1:
            return heirs, self.FARDH_ONE_SIXTH, None

        if len(heirs) == 1:
            return heirs, self.FARDH_ONE_HALF, None, [QURAN['an-Nisa:176:2']]
        else:
            return heirs, self.FARDH_TWO_THIRD, None, [QURAN['an-Nisa:176:4']]

    # Anak laki-laki dari saudara laki-laki sekandung (keponakan)
    # walaupun  jauh (seperti anak dari keponakan)
    def handle_son_of_brothers(self) -> AshhabFardh:
        try:
            sons = self.get_sons_of(self._decedent.get_brothers())
            heirs = self.filter_alive(sons)
        except UnableToFindRelativeError:
            raise UnableToFindLivingRelativeError

        self._mahjub_hirman([
            self.TYPE_SON_OF_PATERNAL_BROTHER,
            self.TYPE_FATHERS_BROTHER,
            self.TYPE_FATHERS_PATERNAL_BROTHER,
            self.TYPE_SON_OF_FATHERS_BROTHER,
            self.TYPE_SON_OF_FATHERS_PATERNAL_BROTHER
        ])

        return heirs, None, self.FARDH_TWO_PART, [QURAN['an-Nisa:11:2'], IJTIMA['ashobah-bin-nafs:3']]

    # Anak laki-laki dari saudara laki-laki sebapak (keponakan)
    def handle_son_of_paternal_brothers(self) -> AshhabFardh:
        try:
            paternal_brother = self._decedent.get_paternal_brothers()
            sons = self.get_sons_of(paternal_brother)
            heirs = self.filter_alive(sons)
        except UnableToFindRelativeError:
            raise UnableToFindLivingRelativeError

        self._mahjub_hirman([
            self.TYPE_FATHERS_BROTHER,
            self.TYPE_FATHERS_PATERNAL_BROTHER,
            self.TYPE_SON_OF_FATHERS_BROTHER,
            self.TYPE_SON_OF_FATHERS_PATERNAL_BROTHER
        ])

        return heirs, None, self.FARDH_TWO_PART, [QURAN['an-Nisa:11:2'], IJTIMA['ashobah-bin-nafs:3']]

    # Paman sekandung
    def handle_fathers_brothers(self) -> AshhabFardh:
        try:
            father = self._decedent.get_father()
            fathers_brother = father.get_brothers()
            heirs = self.filter_alive(fathers_brother)
        except UnableToFindRelativeError:
            raise UnableToFindLivingRelativeError

        self._mahjub_hirman([
            self.TYPE_FATHERS_PATERNAL_BROTHER,
            self.TYPE_SON_OF_FATHERS_BROTHER,
            self.TYPE_SON_OF_FATHERS_PATERNAL_BROTHER
        ])

        return heirs, None, self.FARDH_TWO_PART, [QURAN['an-Nisa:11:2'], IJTIMA['ashobah-bin-nafs:4']]

    # Paman sebapak
    def handle_fathers_paternal_brothers(self) -> AshhabFardh:
        try:
            father = self._decedent.get_father()
            fathers_paternal_brothers = father.get_paternal_brothers()
            heirs = self.filter_alive(fathers_paternal_brothers)
        except UnableToFindRelativeError:
            raise UnableToFindLivingRelativeError

        self._mahjub_hirman([
            self.TYPE_SON_OF_FATHERS_BROTHER,
            self.TYPE_SON_OF_FATHERS_PATERNAL_BROTHER
        ])

        return heirs, None, self.FARDH_TWO_PART, [QURAN['an-Nisa:11:2'], IJTIMA['ashobah-bin-nafs:4']]

    # Anak laki-laki dari paman sekandung (sepupu) walaupun jauh
    def handle_son_of_fathers_brothers(self) -> AshhabFardh:
        try:
            father = self._decedent.get_father()
            fathers_brother = father.get_brothers()
            sons = self.get_sons_of(fathers_brother)
            heirs = self.filter_alive(sons)
        except UnableToFindRelativeError:
            raise UnableToFindLivingRelativeError

        self._mahjub_hirman([
            self.TYPE_SON_OF_FATHERS_PATERNAL_BROTHER
        ])

        return heirs, None, self.FARDH_TWO_PART, [QURAN['an-Nisa:11:2'], IJTIMA['ashobah-bin-nafs:4']]

    # Anak laki-laki dari paman sebapak (sepupu) walaupun jauh
    def handle_son_of_fathers_paternal_brothers(self) -> AshhabFardh:
        try:
            father = self._decedent.get_father()
            fathers_paternal_brothers = father.get_paternal_brothers()
            sons = self.get_sons_of(fathers_paternal_brothers)
            heirs = self.filter_alive(sons)
        except UnableToFindRelativeError:
            raise UnableToFindLivingRelativeError

        return heirs, None, self.FARDH_TWO_PART, [QURAN['an-Nisa:11:2'], IJTIMA['ashobah-bin-nafs:4']]
