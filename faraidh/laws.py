QURAN = {
    'an-Nisa:11:1':
        "Allah mensyariatkan (mewajibkan) kepadamu tentang (pembagian warisan untuk) anak-anakmu, ",
    'an-Nisa:11:2':
        "(yaitu) bagian seorang anak laki-laki sama dengan bagian dua orang anak perempuan. ",
    'an-Nisa:11:3':
        "Dan jika anak itu semuanya perempuan yang jumlahnya lebih dari dua, "
        "maka bagian mereka dua pertiga dari harta yang ditinggalkan. ",
    'an-Nisa:11:4':
        "Jika dia (anak perempuan) itu seorang saja, maka dia memperoleh setengah (harta yang ditinggalkan). ",
    'an-Nisa:11:5':
        "Dan untuk kedua ibu-bapak, bagian masing-masing seperenam dari harta yang ditinggalkan, "
        "jika dia (yang meninggal) mempunyai anak. ",
    'an-Nisa:11:6':
        "Jika dia (yang meninggal) tidak mempunyai anak dan dia diwarisi oleh kedua ibu-bapaknya (saja), "
        "maka ibunya mendapat sepertiga. ",
    'an-Nisa:11:7':
        "Jika dia (yang meninggal) mempunyai beberapa saudara, maka ibunya mendapat seperenam. ",
    'an-Nisa:11:8':
        "(Pembagian-pembagian tersebut di atas) setelah (dipenuhi) wasiat yang dibuatnya atau (dan setelah dibayar) "
        "hutangnya. ",
    'an-Nisa:11:9':
        "(Tentang) orang tuamu dan anak-anakmu, kamu tidak mengetahui siapa di antara mereka yang lebih banyak "
        "manfaatnya bagimu. Ini adalah ketetapan Allah. Sungguh, Allah Maha Mengetahui, Mahabijaksana. ",

    'an-Nisa:12:1':
        "Dan bagianmu (suami-suami) adalah seperdua dari harta yang ditinggalkan oleh istri-istrimu, jika mereka "
        "tidak mempunyai anak.",
    'an-Nisa:12:2':
        "Jika mereka (istri-istrimu) itu mempunyai anak, maka kamu mendapat seperempat dari harta yang ditinggalkannya "
        "setelah (dipenuhi) wasiat yang mereka buat atau (dan setelah dibayar) hutangnya.",
    'an-Nisa:12:3':
        "Para istri memperoleh seperempat harta yang kamu tinggalkan jika kamu tidak mempunyai anak.",
    'an-Nisa:12:4':
        "Jika kamu mempunyai anak, maka para istri memperoleh seperdelapan dari harta yang kamu tinggalkan "
        "(setelah dipenuhi) wasiat yang kamu buat atau (dan setelah dibayar) hutang-hutangmu.",
    'an-Nisa:12:5':
        "Jika seseorang mati baik laki-laki maupun perempuan yang tidak meninggalkan ayah dan tidak "
        "meninggalkan anak, tetapi mempunyai seorang saudara laki-laki (seibu saja) atau seorang saudara "
        "perempuan (seibu saja), ...",
    'an-Nisa:12:6':
        "... maka bagi masing-masing dari kedua jenis saudara (seibu) itu seperenam harta. ",
    'an-Nisa:12:7':
        "... Tetapi jika saudara-saudara seibu itu lebih dari seorang, maka mereka bersekutu dalam yang sepertiga itu "
        "setelah (dipenuhi wasiat) yang dibuatnya atau (dan setelah dibayar) hutangnya dengan tidak "
        "menyusahkan (kepada ahli waris). ",
    'an-Nisa:12:8':
        "Demikianlah ketentuan Allah. Allah Maha Mengetahui, Maha Penyantun.",

    'an-Nisa:176:1':
        "Mereka meminta fatwa kepadamu (tentang kalalah). Katakanlah, “Allah memberi fatwa kepadamu tentang kalalah "
        "(yaitu), ",
    'an-Nisa:176:2':
        "jika seseorang mati dan dia tidak mempunyai anak tetapi mempunyai saudara perempuan, maka bagiannya "
        "(saudara perempuannya itu) seperdua dari harta yang ditinggalkannya, ",
    'an-Nisa:176:3':
        "dan saudaranya yang laki-laki mewarisi (seluruh harta saudara perempuan), jika dia tidak mempunyai anak. ",
    'an-Nisa:176:4':
        "Tetapi jika saudara perempuan itu dua orang, maka bagi keduanya dua pertiga dari harta yang ditinggalkan. ",
    'an-Nisa:176:5':
        "Dan jika mereka (ahli waris itu terdiri dari) saudara-saudara laki-laki dan perempuan, "
        "maka bagian seorang saudara laki-laki sama dengan bagian dua saudara perempuan. ",
    'an-Nisa:176:6':
        "Allah menerangkan (hukum ini) kepadamu, agar kamu tidak sesat. Allah Maha Mengetahui segala sesuatu."

}

HADITS = {
    'bukhari:1':
        "Bagikanlah harta peninggalan (warisan) kepada yang berhak, dan apa yang tersisa menjadi hak laki-laki yang "
        "paling utama. ",
    'ibn-masud:1:1':
        "Ibnu Mas'ud berkata: 'Aku akan memutuskan seperti apa yang pernah diputuskan Rasulullah saw., ",
    'ibn-masud:1:2':
        "bagi anak perempuan separo (1/2) harta peninggalan pewaris, ",
    'ibn-masud:1:3':
        "dan bagi cucu perempuan keturunan dari anak laki-laki mendapat bagian seperenam (1/6) sebagai pelengkap 2/3, ",
    'ibn-masud:1:4':
        "dan sisanya menjadi bagian saudara perempuan kandung atau seayah. "
}

IJTIMA = {
    'hijab':
        "Penghapusan hak waris seseorang, baik penghapusan sama sekali ataupun pengurangan bagian harta warisan "
        "karena ada ahli waris yang lebih dekat pertaliaannya (hubungannya) dengan orang yang meninggal.",
    'hijab-hirman':
        "Penghapusan seluruh bagian, karena ada ahli waris yang lebih dekat hubungannya dengan orang yang meninggal.",
    'hijab-nuqshan':
        "Pengurangan bagian dari harta warisan, karena ada ahli waris lain yang membersamai.",
    'hijab-nuqshan:1':
        "Suami/istri dapat terhijab nuqshan oleh anak laki-laki dan cucu laki-laki.",
    # hijab nuqshan untuk bapak karena anak/cucu ada beberapa pendapat:
    # - https://www.bacaanmadani.com/2018/01/pengertian-hijab-dan-macam-macam-hijab.html
    # - http://media.isnet.org/kmi/islam/Waris/Seper6.html
    'hijab-nuqshan:2a':
        "Bapak dapat terhijab nuqshan oleh anak laki-laki dan cucu laki-laki.",
    'hijab-nuqshan:2b':
        "Bapak dapat terhijab nuqshan oleh anak, baik anak laki-laki atau anak perempuan",
    # hijab nuqshan untuk ibu karena anak/cucu, ada beberapa pendapat:
    # - @see https://www.bacaanmadani.com/2018/01/pengertian-hijab-dan-macam-macam-hijab.html
    # - @see https://fulaih.wordpress.com/2007/11/16/faroid/
    # - @see http://media.isnet.org/kmi/islam/Waris/Seper6.html
    'hijab-nuqshan:3a':
        "Ibu dapat terḥijab nuqshan oleh anak laki-laki, cucu laki-laki",
    'hijab-nuqshan:3b':
        "Ibu dapat terḥijab nuqshan oleh Far’ul Warits (anak/cucu) yang meninggal baik laki-laki maupun Perempuan",
    'hijab-nuqshan:3c':
        "Ibu dapat terḥijab nuqshan oleh anak laki-laki atau perempuan atau cucu laki-laki keturunan anak laki-laki.",
    'hijab-nuqshan:4':
        "Ibu dapat terḥijab nuqshan oleh 2 orang saudara baik sekandung, seayah, seibu atau campuran berapapun jumlahnya",

    'ashobah-bin-nafs':
        "laki-laki yang nasabnya kepada pewaris tidak tercampuri kaum wanita",
    'ashobah-bin-nafs:1':
        "Arah anak, mencakup seluruh laki-laki keturunan anak laki-laki mulai cucu, cicit, dan seterusnya",
    'ashobah-bin-nafs:2':
        "Arah bapak, mencakup ayah, kakek, dan seterusnya, yang pasti hanya dari pihak laki-laki. "
        "Misalnya ayah dari bapak, ayah dari kakak, dan seterusnya",
    'ashobah-bin-nafs:3':
        "Arah saudara laki-laki, mencakup saudara kandung laki-laki, saudara laki-laki seayah, anak laki-laki "
        "keturunan saudara kandung laki-laki, anak laki-laki keturunan saudara laki-laki seayah, dan seterusnya. "
        "Arah ini hanya terbatas pada saudara kandung laki-laki dan yang seayah, termasuk keturunan mereka, "
        "namun hanya yang laki-laki. Adapun saudara laki-laki yang seibu tidak termasuk 'ashabah disebabkan mereka "
        "termasuk ashhabul furudh",
    'ashobah-bin-nafs:4':
        "Arah paman, mencakup paman (saudara laki-laki ayah) kandung maupun yang seayah, termasuk keturunan mereka, "
        "dan seterusnya",

    'ashobah-bi-ghairihi:1':
        "Anak perempuan, akan menjadi 'ashabah bila bersamaan dengan saudara laki-lakinya (yakni anak laki-laki).",
    'ashobah-bi-ghairihi:2':
        "Cucu perempuan keturunan anak laki-laki akan menjadi 'ashabah bila berbarengan dengan saudara laki-lakinya, "
        "atau anak laki-laki pamannya (yakni cucu laki-laki keturunan anak laki-laki), "
        "baik sederajat dengannya atau bahkan lebih di bawahnya.",
    'ashobah-bi-ghairihi:3':
        "Saudara kandung perempuan akan menjadi 'ashabah bila bersama saudara kandung laki-laki",
    'ashobah-bi-ghairihi:4':
        "Saudara perempuan seayah akan menjadi 'ashabah bila bersamaan dengan saudara laki-lakinya, "
        "dan pembagiannya, bagian laki-laki dua kali lipat bagian perempuan",

    'ashobah-maal-ghair':
        "saudara kandung perempuan maupun saudara perempuan seayah, apabila mewarisi "
        "bersamaan dengan anak perempuan yang tidak mempunyai saudara laki-laki",

    # @see http://media.isnet.org/kmi/islam/Waris/Umariyyatan.html
    'umariyyatan':
        "Ibu hanya diberi sepertiga bagian dari SISA harta warisan yang ada, "
        "setelah sebelumnya dikurangi bagian suami atau istri.",


}
