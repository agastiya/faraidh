from typing import Dict, Tuple, Set

from .ashhaabul_furuudh import AshhaabulFuruudh, Fardh, AshhabFardh, Ashhab


class FaraidhSettlement:
    """
    Tata Cara Pembagian Harta Warisan dalam Islam
    http://www.nu.or.id/post/read/87201/tata-cara-pembagian-harta-warisan-dalam-islam
    """

    TYPE_ASHOBAH_UNSHARED = 998
    TYPE_RAD_UNSHARED = 999

    @staticmethod
    def _calculate_least_common_multiple(input_set: Set[int]) -> int:
        """
        Calculate least common multiple (LCM) dengan cara table
        @see https://en.wikipedia.org/wiki/Least_common_multiple

        :param input_set:
        :return:
        """

        asal_masalah = 1
        denominator = 2
        while input_set:
            result_list = []
            is_denominator_applied = False
            for numerator in input_set:
                if numerator % denominator == 0:
                    is_denominator_applied = True
                    result = numerator / denominator
                    if result > 1:
                        result_list.append(result)
                else:
                    result_list.append(numerator)
            if is_denominator_applied:
                asal_masalah = asal_masalah * denominator
            else:
                denominator = 3
            input_set = result_list
        return asal_masalah

    @staticmethod
    def _share(ashhaab: Dict[int, Fardh], asal_masalah: int) -> Tuple[Dict[int, Fardh], int]:
        shared = {}
        total_share = 0
        for k, fardh in ashhaab.items():
            my_share = asal_masalah * fardh[0] // fardh[1]
            shared[k] = my_share, asal_masalah
            total_share += my_share
        return shared, total_share

    @staticmethod
    def _change_asal_masalah(shared: Dict[int, Fardh], asal_masalah: int):
        """jumlah saham/share dijadikan asal masalah yang baru, pengganti yang lama

        :param shared:
        :param asal_masalah:
        :return:
        """
        for k, fardh in shared.items():
            shared[k] = fardh[0], asal_masalah

    def __init__(self, ashhaabul_furud_result: Dict[int, AshhabFardh]):
        self._ashhaabul_furuudh_result = {}
        self._ashobah_result = {}
        self.has_ashhaabul_furud = False
        self.has_ashobah = False
        self.has_rad = False

        self._fardh_set = set()
        for relation, afr in ashhaabul_furud_result.items():
            fardh = afr[AshhaabulFuruudh.ASHHABFARDH_KEY_FARDH]
            if fardh:
                self.has_ashhaabul_furud = True
                self._ashhaabul_furuudh_result[relation] = fardh

                # tamatsul (beberapa kusur (pecahan) yang maqamnya sama)
                # Misalnya 1/2 dengan 1/2, maka diambil salah satu dari maqam tersebut
                # sebagai asal masalah, yaitu 2.
                self._fardh_set.add(fardh[1])

            if afr[AshhaabulFuruudh.ASHHABFARDH_KEY_ASHOBAH]:
                self._ashobah_result[relation] = afr[AshhaabulFuruudh.ASHHABFARDH_KEY_ASHOBAH]

    def tashil(self) -> int:
        """
        ta'shil: mencari asal_masalah, ditetapkan dan digunakan apabila ahli warisnya terdiri dari ahli waris yang
        memiliki bagian pasti atau dzawil furûdl

        Asal Masalah adalah Bilangan terkecil yang darinya bisa didapatkan bagian masing-masing ahli waris secara
        benar tanpa adanya pecahan.” (Musthafa Al-Khin, 2013:339)
        """

        if self.has_ashhaabul_furud:
            if len(self._fardh_set) == 1:
                # terdiri dari ashabul furudh saja, dan hanya satu macam.
                # ada tujuh, yaitu 2, 3, 4, 6, 8
                asal_masalah = next(iter(self._fardh_set))
            else:
                # jika ashabul furudh lebih dari satu macam.
                # ada tujuh kemungkinan, yaitu 2, 3, 4, 6, 8, 12 dan 24.
                asal_masalah = self._calculate_least_common_multiple(self._fardh_set)

        else:
            asal_masalah = self._tashil_ashobah()

        return asal_masalah

    def _tashil_ashobah(self):
        """
        Mencari asal masalah untuk ashobah, dibentuk melalui Adadur Ru’ûs yang berarti bilangan kepala/jumlah
        kepala/orang yang menerima warisan.

        FIXME: saat ini tidak menghitung kepala

        :return:
        """
        asal_masalah = 0
        for k, fardh in self._ashobah_result.items():
            asal_masalah += fardh[0]
        return asal_masalah

    def share_ashhaabul_furuudh(self, asal_masalah: int) -> Dict[int, Fardh]:
        """Tashihul Faraa'idh (pengesahan bagian)"""

        shared, total_share = self._share(self._ashhaabul_furuudh_result, asal_masalah)

        # 'Aul and Rad
        # @see https://yufidia.com/belajar-ilmu-waris-fiqh-faraidh-bag-4/
        if total_share > asal_masalah:
            # ‘Aul maksudnya naiknya angka pada masalah
            # ketika dijumlahkan seluruh bagian yang akan didapatkan
            # oleh ahli waris.
            if asal_masalah == 6 and total_share <= 10:
                self._change_asal_masalah(shared, total_share)
            elif asal_masalah == 12 and total_share <= 17:
                self._change_asal_masalah(shared, total_share)
            elif asal_masalah == 24 and total_share == 27:
                self._change_asal_masalah(shared, total_share)

        elif total_share < asal_masalah:

            if len(self._ashobah_result) > 0:
                shared[self.TYPE_ASHOBAH_UNSHARED] = (asal_masalah - total_share, asal_masalah)
                self.has_ashobah = True

            else:
                if AshhaabulFuruudh.TYPE_WIFE not in self._ashhaabul_furuudh_result and \
                        AshhaabulFuruudh.TYPE_HUSBAND not in self._ashhaabul_furuudh_result:
                    # semua ahli waris berhak menerima Radd, maka penyelesaiannya:
                    # Jumlah saham tersebut dijadikan asal masalah yang baru pengganti AM lama.
                    self._change_asal_masalah(shared, total_share)

                else:
                    # Jika di antara ahli waris ada yang tidak berhak menerima Radd, maka
                    # cara penyelesaiannya adalah:
                    # – Sisa harta hanya diberikan kepada mereka yang berhak menerima saja
                    #   sesuai fardhnya masing-masing.
                    # – Menjumlahkan sisa penerimaan mereka dengan penerimaan mereka semula.
                    shared[self.TYPE_RAD_UNSHARED] = (asal_masalah - total_share, asal_masalah)
                    self.has_rad = True

        return shared

    def share_rad(self) -> Dict[int, Fardh]:
        """Sisa harta hanya diberikan kepada mereka yang berhak menerima saja sesuai fardhnya masing-masing.

        :return:
        """
        shared = {}
        if self.has_rad:
            fardh_set = set()
            for k, fardh in self._ashhaabul_furuudh_result.items():
                if k in [AshhaabulFuruudh.TYPE_WIFE, AshhaabulFuruudh.TYPE_HUSBAND]:
                    continue
                shared[k] = fardh
                fardh_set.add(fardh[1])

            shared, asal_masalah = self._share(shared, self._calculate_least_common_multiple(fardh_set))
            self._change_asal_masalah(shared, asal_masalah)

        return shared

    def share_ashobah(self) -> Dict[int, Fardh]:
        if self.has_ashobah:
            asal_masalah = self._tashil_ashobah()
            self._change_asal_masalah(self._ashobah_result, asal_masalah)
        return self._ashobah_result
