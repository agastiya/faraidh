from ged4py import GedcomReader
from ged4py import model
from collections import deque

import click


@click.command()
@click.option('--src', prompt='GEDCOM filename', help='The GEDCOM filename to read.')
@click.option('--name', default=None, help='Only show related family from one individual name.')
def hello(src, name):
    """Simple program that read FILENAME and print all names in the file."""
    with GedcomReader(src) as parser:
        records: [model.Individual] = parser.records0('INDI')
        for record in records:
            if name is not None and name != record.name.format():
                continue
            show_individual(record, 'ROOT')
            set_shown(record)
            show_relations_of(record)


def showing_deferred_when_not_shown(showing, record, title, exception=None):
    """

    :type showing: deque
    :type record: model.Individual
    :type title: str
    :type exception: model.Individual
    """
    if record and (exception is None or exception.offset != record.offset):
        show_individual(record, title)
        if not is_shown(record):
            set_shown(record)
            showing.append(record)


def showing_flush_deferred(showing):
    """

    :type showing: deque
    """
    while showing:
        record: model.Individual = showing.popleft()
        show_relations_of(record)


def show_records(records, title, exception=None):
    """

    :type records: [model.Individual]
    :type title: str
    :type exception: model.Individual
    """
    if not records:
        return
    showing = deque([])
    for record in records:
        if record.offset == exception:
            # print(title)
            # print(record.name.format(), "is in exception")
            # print()
            pass
        elif is_shown(record):
            print(title)
            print(record.name.format(), "is already shown")
            print()
        else:
            showing_deferred_when_not_shown(showing, record, title)
    showing_flush_deferred(showing)


def show_relations_of(record):
    """

    :type record: model.Individual
    """
    # print(record.sub_records)

    showing = deque([])

    name = record.name.format()

    fam_pointer: model.Pointer = record.sub_tag('FAMC')
    if fam_pointer:
        # print("Going up from ", name)
        husband = fam_pointer.sub_tag('HUSB')
        wife = fam_pointer.sub_tag('WIFE')
        if husband or wife:
            print('Parents of', name)
            show_event(fam_pointer, 'MARR', 'Marriage')
            print()
            showing_deferred_when_not_shown(showing, husband, 'Father of ' + name)
            showing_deferred_when_not_shown(showing, wife, 'Mother of ' + name)
        children = fam_pointer.sub_tags('CHIL')
        show_records(children, 'Siblings of ' + name, record.offset)

    fam_pointer = record.sub_tag('FAMS')
    if fam_pointer:
        # print("Going down from ", name)
        husband = fam_pointer.sub_tag('HUSB')
        wife = fam_pointer.sub_tag('WIFE')
        if husband or wife:
            show_event(fam_pointer, 'MARR', 'Married to ' + name)
            showing_deferred_when_not_shown(showing, husband, 'Husband of ' + name, record)
            showing_deferred_when_not_shown(showing, wife, 'Wife of ' + name, record)
        children = fam_pointer.sub_tags('CHIL')
        show_records(children, 'Children of ' + name)

    showing_flush_deferred(showing)


def is_shown(record):
    """


    :type record: model.Individual
    :return: bool
    """
    return record.offset in record_shown


def set_shown(record):
    """

    :type record: model.Individual
    """
    record_shown.append(record.offset)


def show_individual(record, title=None):
    """

    :type record: model.Individual
    :type title: str
    """
    if title:
        print(title)
    if is_shown(record):
        print(record.name.format(), "is already shown")
    else:
        if record.sub_tag('TITL'):
            print('Title:', record.sub_tag_value('TITL'))
        print('Name:', record.name.format())
        print('Sex:', record.sex)
        show_event(record, 'BIRT', 'Birth')
        show_event(record, 'DEAT', 'Death')
    print()


def show_event(record, tag, title):
    """

    :type record: [model.Individual,model.Pointer]
    :type tag: str
    :type title: str
    """
    place: str = record.sub_tag_value(tag + '/PLAC')
    date: model.DateValue = record.sub_tag_value(tag + '/DATE')
    if place and date:
        print(title + ':', place + ',', date.fmt())
    elif date:
        print(title, 'since', date.fmt())
    elif place:
        print(title, 'at', place)


if __name__ == '__main__':
    record_shown = []
    hello()
